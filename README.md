# Telegram Bot Framework by Abfelbaum


## Abstract

This framework aims to build extensible bots and simultaneously be easily extensible.

A small warning: It may not be as easy to get started with this one as with other frameworks. I really recommend reading
the documentation and have a look at the examples before starting to develop with this library.

## Getting started

For basic examples have a look at the `examples` folder. The examples there cover the most used and most important
features.

### Setup

#### Console Application

##### TelegramApplicationBuilder

This library contains an application builder that was heavily inspired by the new ASP.NET Core WebApplicationBuilder.

To use it, install the `Abfelbaum.Telegram.Bot.Framework.Hosting` package and configure it in your `Program.cs`.

An example `Program.cs` can be found
[here](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/LongPolling.Echo/Program.cs)
.

This installation method is meant for applications that are only made for Telegram Bots. If you need more then that in
your application, you should have a look at the *Other* section.

##### Other

If you need to use this library in a more complex scenario, you can just use the normal host builder to build your
application.

To add standard Telegram services to the ServiceCollection
use `IServiceCollection.AddTelegramDependencyInjectionServices()`
in `Abfelbaum.Telegram.Bot.Framework.DependencyInjection`.

This will return a TelegramApplicationBuilder that you can use to setup your Telegram bot. An example can be found
[here](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/LongPollingHosting.Echo/Program.cs)
.

#### With ASP.NET Core

##### WebApplicationBuilder

To use it, install the `Abfelbaum.Telegram.Bot.Framework.AspNetCore` package and configure it in your `Program.cs`.

An example `Program.cs` can be found
[here](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/WebHook.Echo/Program.cs)
.

When you are using a `Startup` class, have a look at the *Other* section.

##### Other

To add standard Telegram services to the ServiceCollection
use `IServiceCollection.AddTelegramDependencyInjectionServices()`
in `Abfelbaum.Telegram.Bot.Framework.DependencyInjection`.

This will return a TelegramApplicationBuilder that you can use to setup your Telegram bot. An example can be found
[here](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/LongPollingHosting.Echo/Program.cs)
.

#### ApplicationBuilder

##### Services

When you have your `TelegramApplicationBuilder` you can use various methods to setup your Telegram bot and it's update
pipeline.

The most important part of setting up your Telegram bot is calling `IServiceCollection.AddBot<TContext>` for adding
the `ITelegramBotClient` to dependency injection and passing the bot token down.

After that, you may want to register some updates. `ITelegramApplicationBuilder.AddUpdate<TUpdate>()`
and `ITelegramApplicationBuilder.AddCommand<TCommand>()` (in `Abfelbaum.Telegram.Bot.Framework.Commands`) let you do
that.

When you registered all necessary services, you can begin to add your middlewares. Technically you could do that in
between registering services, but that makes your code less clear. You should have at least *some* structure in there!

##### Middlewares

In some cases (everything based on `Abfelbaum.Telegram.Bot.Framework.DependencyInjection`) you can add middlewares
immediately. In other cases (everything based on `Abfelbaum.Telegram.Bot.Framework.Hosting`) you have to build
the `TelegramApplication` first.

After that, you can just add middlewares as you'd like to the pipeline. Please note that middlewares are getting
executed in the order they are registered. You should keep an eye on registering low level middlewares that need to be
called as soon as possible (i.e. `SequentialInputs`, `RequestCulture`, `ConcurrentUpdates`) before higher level
middlewares (i.e. `UpdateHandlingMiddleware`).

Please note:

When you are writing custom middlewares that contain code branches that do not call the `NextMiddleware` `delegate`,
please call `await context.RequestEnded();` before returning. Otherwise, some resources may not be cleaned up by the
garbage collector.

If your middleware contains some stuff that should be disposed after the request ended, add a callback method
to `context.RequestEndedActions` that takes care of the disposal.

The callback methods there are getting called on a `context.RequestEnded()` call.

## Features

### Dependency Injection

Everything you need is accessible via dependency injection!

On every request a new service scope is created that contains the context for your current request.

I'll try to provide a mostly complete list for stuff that you can use here, but i cannot promise anything.

| Type                             | Lifetime    | Purpose                           |
| -------------------------------- | ----------- | --------------------------------- |
| `ITelegramBotClient`             | `Scoped`    | Talking to Telegram API           |
| `DefaultContext` and  `TContext` | `Scoped`    | Telegram request context          |
| `IBotService`                    | `Singleton` | Getting information about the bot |
| `IOptions<BotOptions>`           | `Singleton` | Bot configuration (i.e. Token)    |

### Update settings

Update settings provide various information to the framework that can be used to improve overall development experience.

They also should be as lightweight as possible, since their lifetime is going to be singleton.

The framework ships UpdateSettings for every update type. This should cover most use cases and you most likely won't
work with `RequiredUpdates` and `Preprocessors`.

Lifetime: singleton, living outside of a scope

#### RequiredUpdates

The update types needed have to be specified here. The framework collects the update types of all update settings and
tells Telegram to only send necessary updates to the bot. This saves computing power on your and Telegrams side.

As an additional benefit this acts as the first filter mechanism for your update handlers: If the provided update types
do not match the update type of the current update, no further steps are needed to prevent execution of that specific
update handler.

#### Handlers

The handler dictionary should contain any update handlers the settings can execute.

Any handler must have a unique identifier. It is recommended to create a local enum with values for each handler.

#### GetUpdateHandlerIdentifier

The GetUpdateHandlerIdentifier should provide the update handler for the current update. If you don't want any to be
executed, just return null.

### Preprocessors

Preprocessors make it possible to preprocess (i. e. extract Message object of a Message update) for every handler in the
update settings where it is specified.

To implement your own preprocessor, create a class and implement `IUpdatePreprocessor`.

Lifetime: transient, living in a scope

### UpdateHandlers

Update handlers handle an update. They have to implement `ITelegramUpdate<T>`, where `T` is either `Update` or the
output type of the preprocessor.

Lifetime: transient, living in a scope

## Commands

Contained in package: `Abfelbaum.Telegram.Bot.Framework.Commands`.

CommandSettings (the settings base class) are just fancy MessageSettings.

Inherit from `CommandSettings` and you'll be provided with some settings for your command.

### CommandUpdater

The CommandUpdater can scan all your commands and tell Telegram about them.

To use it add `BotBuilder<TContext>.Services.AddCommandUpdater()` to your BotBuilder.

### HelpCommand

The help command provides a help page that lists all your commands.

To activate it, add `BotBuilder<TContext>.Updates.AddHelpCommand()` to your BotBuilder.

You can access HelpCommand texts by injecting `ICommandHelpGenerator` into your services. Feel free to play around with
them!

### CommandSettings

To use register them to the bot use `ITelegramApplicationBuilder.AddCommand<TSettings>();`.

If you do have a command with only one handler, you can use
the `ITelegramApplicationBuilder.AddSimpleCommand<THandler>()` method to register it. The settings below can be passed
to that method.

#### Name

The name of your command. The command name should only contain `a-z`, `0-9` and `_`.

#### ShortDescription

Should contain a short description of your command. This is used for the `/help` overview page.

#### LongDescription

Should contain the long description of your command with all side effects etc.

It is used for the detail help page.

#### Usages

An array with examples how to use the command.

A few tips:

- Wrapping words in `<>` tells the user that this is a mandatory parameter
- Wrapping words in `[]` tells the user that this is am optional parameter

An example:

`/command <required> [optional]`

### LongPolling

Contained in package: `Abfelbaum.Telegram.Bot.Framework.Polling`.

The polling package provides longpolling for updates from Telegram.

To use it, just add `ITelegramApplicationBuilder.AddPolling()` to your BotBuilder.

### WebHook

Contained in package: `Abfelbaum.Telegram.Bot.Framework.WebHook`.

To learn how to use webhooks, you should have a look at
the [webhook example](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/WebHook.Echo)
.

### RateLimiting

Contained in package: `Abfelbaum.Telegram.Bot.Framework.RateLimiting`.

The request scheduler in this package ensures that you won't hit Telegram rate limits.

It delays requests until it is safe to make them.

To use it, add `ITelegramApplicationBuilder.AddRequestScheduler().`.

When you use the request scheduler ensure to add this one to the BotBuilder before any other Telegram related stuff!

### RequestCulture

Contained in package: `Abfelbaum.Telegram.Bot.Framework.RequestCulture`.

This package contains a middleware that tries to determine the culture of the current update.

If no culture is found, it falls back to a default culture (en) that can be configured
via `ITelegramApplicationBuilder.Services.Configure<RequestCultureOptions>(options => options.DefaultCulture = CultureInfo.GetCulture("en"));`
.

### SequentialUpdates

Contained in package: `Abfelbaum.Telegram.Bot.Framework.SequentialUpdates`.

This was heavily inspired by https://github.com/wiz0u/YourEasyBot/ even though I coded everything by myself. Many
credits and thanks to wiz0u for that project and the idea!

Sequential updates enable you to wait for specific updates like you would wait for console inputs. This simplifies the
development of bots that wait for specific user inputs.

To use sequential updates, inject the `ISequentialInputService` and use its only method to specify the conditions for
the update.

An example can be
found [here](https://git.thevillage.chat/Abfelbaum/abfelbaum.telegram.framework/-/blob/main/examples/LongPolling.SequentialEcho/SequentialEchoCommandHandler.cs).