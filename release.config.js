const branch = process.env.CI_COMMIT_BRANCH
const isStableRelease = process.env.STABLE_RELEASE === "true";

const config = {
  tagFormat: "${version}",
  branches: [
    "+([0-9])?(.{+([0-9]),x}).x",
    "main",
    {
      name: "dev",
      channel: "default",
      prerelease: isStableRelease ? false : "beta"
    }
  ],
  plugins: [
    ["@semantic-release/commit-analyzer", {
      preset: "conventionalcommits"
    }]
  ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {

  config.plugins.push(["@semantic-release/release-notes-generator", {
    preset: "conventionalcommits"
  }]);

  config.plugins.push([ "@semantic-release/gitlab", {
    labels: "Type::Bugfix,Priority::High"
  }]);

  config.plugins.push([ "@semantic-release/changelog", {
    changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
  }]);

  config.plugins.push([ "@semantic-release/git", {
    message: "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes} [skip ci]"
  }]);
}

if(branch !== "main" && branch !== "dev") {
  config.branches.push({
    name: branch,
    channel: "default",
    prerelease: "alpha"
  })
}

module.exports = config