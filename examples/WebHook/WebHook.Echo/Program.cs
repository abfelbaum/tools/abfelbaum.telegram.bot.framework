using Abfelbaum.Telegram.Bot.Framework.AspNetCore;
using Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Configuration;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.RateLimiting;
using Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Abfelbaum.Telegram.Bot.Framework.WebHook;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting.StaticWebAssets;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Telegram.Bot.Types.Enums;
using WebHook.Echo;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

StaticWebAssetsLoader.UseStaticWebAssets(builder.Environment, builder.Configuration);

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Adds core services for interacting with Telegram API
TelegramApplicationBuilder<DefaultContext> telegramBuilder = builder.AddTelegramAspNetServices<DefaultContext>();

// Adds services for Telegram Bots
telegramBuilder.Services.AddBot(builder.Configuration.GetSection("Bot"));

// Adding request scheduler so you won't hit rate limits
telegramBuilder.Services.AddRequestScheduler();

// Adding webhook support
telegramBuilder.Services.AddWebHook(builder.Configuration.GetSection("Bot:WebHook"));

// Adding UpdateSettings to BotBuilder
telegramBuilder.AddUpdate<EchoUpdate>(UpdateType.Message);

telegramBuilder.Services.AddRequestCulture();
telegramBuilder.Services.AddSequentialInputs();
telegramBuilder.Services.AddConcurrentUpdates();

telegramBuilder.UseRequestCulture();
telegramBuilder.UseSequentialInputs();
telegramBuilder.UseConcurrentUpdates();
telegramBuilder.UseUpdateHandling();

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseAuthorization();

// Configure custom endpoint per Telegram API recommendations:
// https://core.telegram.org/bots/api#setwebhook
// If you'd like to make sure that the Webhook request comes from Telegram, we recommend
// using a secret path in the URL, e.g. https://www.example.com/<token>.
// Since nobody else knows your bot's token, you can be pretty sure it's us.
BotOptions botOptions = app.Services.GetRequiredService<IOptions<BotOptions>>().Value;
app.MapControllerRoute(
    name: "tgwebhook",
    pattern: $"bot/{botOptions.Token}",
    new { controller = "Webhook", action = "Post" }
);
app.MapControllers();
app.MapFallbackToFile("index.html");
app.Run();