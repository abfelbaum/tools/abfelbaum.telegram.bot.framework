using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace WebHook.Echo;

public class AdvancedEchoUpdateHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;

    public AdvancedEchoUpdateHandler(ITelegramBotClient client)
    {
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        await _client.MakeRequestAsync(new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = $"{message.From?.FirstName} at {nameof(AdvancedEchoUpdateHandler)}: {message.Text}",
        }, cancellationToken: cancellationToken);
    }
}