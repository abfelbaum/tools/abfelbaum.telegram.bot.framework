using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.WebHook;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace WebHook.Echo.Controllers;

public class WebhookController : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> Post([FromServices] IWebHookUpdateHandler updateHandler,
        [FromBody] Update update)
    {
        // Use the webhook update handler to start a new update pipeline on every request
        await updateHandler.HandleUpdateAsync(update, CancellationToken.None);

        // Return success status codes
        return Ok();
    }
}