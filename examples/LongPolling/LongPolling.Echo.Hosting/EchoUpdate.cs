using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Updates;
using Telegram.Bot.Types;

namespace LongPolling.Echo.Hosting;

public class EchoUpdate : MessageUpdate
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.Message?.Text is not { } text)
        {
            return Task.FromResult<Type?>(null);
        }

        if (text.Split(' ').Length > 5)
        {
            return Task.FromResult<Type?>(typeof(AdvancedEchoUpdateHandler));
        }

        return Task.FromResult<Type?>(typeof(SimpleEchoUpdateHandler));
    }
}