﻿using Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;
using Abfelbaum.Telegram.Bot.Framework.DependencyInjection;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Polling;
using Abfelbaum.Telegram.Bot.Framework.RateLimiting;
using Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using LongPolling.Echo.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types.Enums;

IHostBuilder hostBuilder = Host.CreateDefaultBuilder(args);

hostBuilder.UseConsoleLifetime();

hostBuilder.ConfigureAppConfiguration((context, configurationBuilder) =>
{
    configurationBuilder.AddEnvironmentVariables();
    configurationBuilder.AddJsonFile("appsettings.json", true);
    configurationBuilder.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", true);
});

hostBuilder.ConfigureLogging(loggingBuilder =>
    loggingBuilder.AddFilter("System.Net.Http.HttpClient", LogLevel.Warning));

hostBuilder.ConfigureServices((context, services) =>
{
    TelegramApplicationBuilder<DefaultContext> telegramBuilder =
        services.AddTelegramDependencyInjectionServices<DefaultContext>(context.HostingEnvironment,
            context.Configuration);
    services.AddBot(context.Configuration.GetSection("Bot"));

    // Adding request scheduler so you won't hit rate limits
    services.AddRequestScheduler();

    // Adding longpolling support
    services.AddPolling();

    // Adding UpdateSettings to BotBuilder
    telegramBuilder.AddUpdate<EchoUpdate>(UpdateType.Message);

    services.AddRequestCulture();
    services.AddSequentialInputs();
    services.AddConcurrentUpdates();

    // Configure pipeline
    telegramBuilder.UseRequestCulture();
    telegramBuilder.UseSequentialInputs();
    telegramBuilder.UseConcurrentUpdates();
    telegramBuilder.UseUpdateHandling();
});

IHost host = hostBuilder.Build();

await host.RunAsync();