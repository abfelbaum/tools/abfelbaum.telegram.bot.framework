using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Updates;
using Telegram.Bot.Types;

namespace LongPolling.Echo;

public class EchoUpdate : MessageUpdate
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.Message?.Text is null)
        {
            return Task.FromResult<Type?>(null);
        }

        if (update.Message.Text.Length > 5)
        {
            return Task.FromResult<Type?>(typeof(AdvancedEchoUpdateHandler));
        }

        return Task.FromResult<Type?>(typeof(SimpleEchoUpdateHandler));
    }
}