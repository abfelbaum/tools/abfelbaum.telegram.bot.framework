using System;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace LongPolling.Echo.Sequential;

public class SequentialEchoCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;
    private readonly ISequentialInputService _input;

    public SequentialEchoCommandHandler(ISequentialInputService input, ITelegramBotClient client)
    {
        _input = input;
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        await _client.MakeRequestAsync(new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = "Please send me a message that i can echo!",
            ReplyParameters = new ReplyParameters
            {
                MessageId = message.MessageId
            }
        }, cancellationToken: cancellationToken);

        Message resultMessage =
            await _input.WaitForMessage(
                message.Chat.Id,
                MessageType.Text,
                resultMessage => resultMessage,
                TimeSpan.FromHours(1),
                userId: message.From?.Id,
                cancellationToken: cancellationToken
            );

        await _client.MakeRequestAsync(new SendMessageRequest
        {
            ChatId = resultMessage.Chat.Id,
            Text = resultMessage.Text!,
            ReplyParameters = new ReplyParameters
            {
                MessageId = resultMessage.MessageId
            }
        }, cancellationToken: cancellationToken);
    }
}