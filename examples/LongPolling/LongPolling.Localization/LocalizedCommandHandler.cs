using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.Extensions.Localization;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace LongPolling.Localization;

public class LocalizedCommandHandler : ITelegramUpdate<Message>
{
    private readonly IStringLocalizer<LocalizedCommandHandler> _localizer;
    private readonly ITelegramBotClient _client;

    public LocalizedCommandHandler(IStringLocalizer<LocalizedCommandHandler> localizer, ITelegramBotClient client)
    {
        _localizer = localizer;
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.From is null)
        {
            return;
        }

        StringBuilder builder = new();
        builder.Append($"{_localizer["information.languagecode"]}: {message.From.LanguageCode}");
        builder.AppendLine();
        builder.Append($"{_localizer["information.culture"]}: {CultureInfo.CurrentCulture.Name}");
        builder.AppendLine();
        builder.Append($"{_localizer["information.uiculture"]}: {CultureInfo.CurrentUICulture.Name}");
        builder.AppendLine();
        builder.Append($"{_localizer["information.defaultculture"]}: {CultureInfo.DefaultThreadCurrentCulture?.Name}");
        builder.AppendLine();
        builder.Append(
            $"{_localizer["information.defaultuiculture"]}: {CultureInfo.DefaultThreadCurrentUICulture?.Name}");

        await _client.MakeRequestAsync(new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = builder.ToString(),
            ReplyParameters = new ReplyParameters
            {
                MessageId = message.MessageId
            }
        }, cancellationToken);
    }
}