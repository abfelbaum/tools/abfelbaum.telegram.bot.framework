using System.Collections.Generic;
using System.Globalization;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Localization;

namespace LongPolling.Localization;

public sealed class LocalizedTelegramCommand : TelegramCommand
{
    public LocalizedTelegramCommand(IBotService botService, IScopeMatchingService scopeMatchingService, IStringLocalizer<LocalizedTelegramCommand> localizer) : base(botService, scopeMatchingService)
    {
        Documentation = new CommandDocumentation
        {
            Name = "command.name",
            Localizer = localizer,
            ShortDescription = "command.description.short",
            LongDescription = "command.description.long",
            Usages = { "command.name" },
            CultureInfos = { CultureInfo.GetCultureInfo("en"), CultureInfo.GetCultureInfo("de"), CultureInfo.GetCultureInfo("fr") }
        };
    }
    public override CommandDocumentation Documentation { get; init; }

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<LocalizedCommandHandler>(CommandScope.Default())
    };
}