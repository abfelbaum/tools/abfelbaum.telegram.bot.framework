﻿// See https://aka.ms/new-console-template for more information

using System.Globalization;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Extensions;
using Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Hosting;
using Abfelbaum.Telegram.Bot.Framework.Polling;
using Abfelbaum.Telegram.Bot.Framework.RateLimiting;
using Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using LongPolling.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

TelegramApplicationBuilder<DefaultContext> builder = TelegramApplication<DefaultContext>.CreateBuilder(args);

builder.Host.UseConsoleLifetime();

builder.Configuration.AddEnvironmentVariables();
builder.Configuration.AddJsonFile("appsettings.json", true);
builder.Configuration.AddJsonFile(
    $"appsettings.{builder.Environment.EnvironmentName}.json", true);

builder.Logging.AddFilter("System.Net.Http.HttpClient", LogLevel.Warning);

builder.Services.AddBot(builder.Configuration.GetSection("Bot"));

// Adding request scheduler so you won't hit rate limits
builder.Services.AddRequestScheduler();

// Adding longpolling support
builder.Services.AddPolling();

// Adding commands to BotBuilder
builder.Services.AddCommandUpdater();
builder.AddHelpCommand();
builder.AddCommand<LocalizedTelegramCommand>();

builder.Services.AddRequestCulture();
builder.Services.AddSequentialInputs();
builder.Services.AddConcurrentUpdates();

TelegramApplication<DefaultContext> app = builder.Build();

app.UseRequestCulture();
app.UseSequentialInputs();
app.UseConcurrentUpdates();
app.UseUpdateHandling();

await app.RunAsync();