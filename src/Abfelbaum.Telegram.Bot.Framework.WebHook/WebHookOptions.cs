namespace Abfelbaum.Telegram.Bot.Framework.WebHook;

public class WebHookOptions
{
    public required string Url { get; set; }
    public bool DropPendingUpdates { get; set; }
    public int? MaxConnections { get; set; }
    public string? SecretToken { get; set; }
}