using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace Abfelbaum.Telegram.Bot.Framework.WebHook;

public static class ServiceCollectionExtensions
{
    public static OptionsBuilder<WebHookOptions> AddWebHook(this IServiceCollection services)
    {
        services.TryAddTransient<IWebHookUpdateHandler, WebHookUpdateHandler>();

        services.AddHostedService<ConfigureWebhook>();
        
        return services.AddOptions<WebHookOptions>();
    }

    public static void AddWebHook(this IServiceCollection services, Action<WebHookOptions> configureOptions)
    {
        OptionsBuilder<WebHookOptions> optionsBuilder = services.AddWebHook();

        optionsBuilder.Configure(configureOptions);
    }

    public static void AddWebHook(this IServiceCollection services, IConfiguration configuration)
    {
        OptionsBuilder<WebHookOptions> optionsBuilder = services.AddWebHook();

        optionsBuilder.Bind(configuration);
    }
}