using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.WebHook;

public interface IWebHookUpdateHandler
{
    public UpdateType[]? AllowedUpdates { get; }
    public Task HandleUpdateAsync(Update update, CancellationToken cancellationToken);
}