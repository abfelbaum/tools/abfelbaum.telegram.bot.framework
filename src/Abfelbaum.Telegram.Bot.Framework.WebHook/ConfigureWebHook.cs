using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Requests;

namespace Abfelbaum.Telegram.Bot.Framework.WebHook;

public class ConfigureWebhook : IHostedService
{
    private readonly ILogger<ConfigureWebhook> _logger;
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly WebHookOptions _webHookOptions;

    public ConfigureWebhook(ILogger<ConfigureWebhook> logger,
        IServiceScopeFactory scopeFactory,
        IOptions<WebHookOptions> webHookOptions)
    {
        _logger = logger;
        _scopeFactory = scopeFactory;
        _webHookOptions = webHookOptions.Value;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        if (_webHookOptions.Url is null)
        {
            throw new ArgumentNullException(nameof(_webHookOptions.Url));
        }

        AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();
        await using ConfiguredAsyncDisposable _ = scope.ConfigureAwait(false);
        ITelegramBotClient botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();
        IRequiredUpdates requiredUpdates = scope.ServiceProvider.GetRequiredService<IRequiredUpdates>();

        _logger.LogInformation("Setting webhook ");
        await botClient.MakeRequestAsync(new SetWebhookRequest
        {
            Url = _webHookOptions.Url,
            MaxConnections = _webHookOptions.MaxConnections,
            SecretToken = _webHookOptions.SecretToken,
            DropPendingUpdates = _webHookOptions.DropPendingUpdates,
            AllowedUpdates = requiredUpdates.ToList()
        }, cancellationToken: cancellationToken).ConfigureAwait(false);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        await using AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();
        ITelegramBotClient botClient = scope.ServiceProvider.GetRequiredService<ITelegramBotClient>();

        // Remove webhook upon app shutdown
        _logger.LogInformation("Removing webhook");
        await botClient.MakeRequestAsync(new DeleteWebhookRequest(), cancellationToken: cancellationToken)
            .ConfigureAwait(false);
    }
}