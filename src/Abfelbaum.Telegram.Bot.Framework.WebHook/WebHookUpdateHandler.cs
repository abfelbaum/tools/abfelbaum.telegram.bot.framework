using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.WebHook;

public class WebHookUpdateHandler : IWebHookUpdateHandler
{
    private readonly IInternalUpdateHandler _updateHandler;

    public WebHookUpdateHandler(IInternalUpdateHandler updateHandler)
    {
        _updateHandler = updateHandler;
    }

    public Task HandleUpdateAsync(Update update, CancellationToken cancellationToken)
    {
        return _updateHandler.HandleUpdateAsync(update, cancellationToken);
    }

    public UpdateType[]? AllowedUpdates => _updateHandler.AllowedUpdates;
}