using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;

public interface IScopeMatchingService
{
    public Task<bool> ScopeContains(CommandScope scope, Chat chat, User? user,
        CancellationToken cancellationToken = new());

    public Task<bool> ScopesContain(IEnumerable<CommandScope> scopes, Chat chat, User? user,
        CancellationToken cancellationToken = new());
}