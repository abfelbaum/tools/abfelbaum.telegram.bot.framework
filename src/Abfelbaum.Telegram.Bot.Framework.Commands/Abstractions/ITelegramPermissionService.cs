using System.Threading;
using System.Threading.Tasks;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;

public interface ITelegramPermissionService
{
    public Task<bool> IsChatAdministrator(long userId, CancellationToken cancellationToken = new());
    public Task<bool> IsChatOwner(long userId, CancellationToken cancellationToken = new());
    public Task<bool> IsChatOwnerOrAdministrator(long userId, CancellationToken cancellationToken = new());
}