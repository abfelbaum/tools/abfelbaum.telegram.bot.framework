using System.Collections.Generic;
using System.Linq;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;

public class SimpleTelegramCommand<THandler> : TelegramCommand
    where THandler : ITelegramUpdate<Message>
{

    public SimpleTelegramCommand(IBotService botService, IScopeMatchingService scopeMatchingService) : base(botService, scopeMatchingService)
    {
    }

    public override required CommandDocumentation Documentation { get; init; }

    public required IList<CommandScope> Scopes { get; init; }
    public CommandHandlerSelector? Selector { get; init; }

    public override IEnumerable<CommandHandler> Handlers => new[]
    {
        new CommandHandler(typeof(THandler), Scopes.ToArray())
        {
            Selector = Selector
        }
    };
}