using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;

public interface ITelegramCommand
{
    public CommandDocumentation Documentation { get; }
    public IEnumerable<CommandHandler> Handlers { get; }
}