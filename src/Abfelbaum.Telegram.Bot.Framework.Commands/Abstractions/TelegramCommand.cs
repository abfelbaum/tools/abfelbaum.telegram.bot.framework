using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Abfelbaum.Telegram.Bot.Framework.Updates;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;

public abstract class TelegramCommand : MessageUpdate, ITelegramCommand
{
    private readonly IBotService _botService;
    private readonly IScopeMatchingService _scopeMatchingService;

    protected TelegramCommand(IBotService botService, IScopeMatchingService scopeMatchingService)
    {
        _botService = botService;
        _scopeMatchingService = scopeMatchingService;
    }

    public abstract CommandDocumentation Documentation { get; init; }
    public abstract IEnumerable<CommandHandler> Handlers { get; }

    protected override async Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (!await IsCommand(update).ConfigureAwait(false))
        {
            return null;
        }

        return await GetHandlerTypes(update).ConfigureAwait(false);
    }

    private async Task<Type?> GetHandlerTypes(Update update)
    {
        var validScopes = new List<CommandHandler>();
        foreach (CommandHandler handler in Handlers)
        {
            if (!handler.Scopes.Any())
            {
                validScopes.Add(new CommandHandler(handler.Type, CommandScope.Default())
                {
                    Selector = handler.Selector
                });
                continue;
            }

            foreach (CommandScope scope in handler.Scopes)
            {
                if (!await _scopeMatchingService.ScopeContains(scope, update.Message!.Chat, update.Message.From)
                        .ConfigureAwait(false))
                {
                    continue;
                }

                validScopes.Add(new CommandHandler(handler.Type, scope)
                {
                    Selector = handler.Selector
                });
            }
        }

        IEnumerable<CommandHandler> results =
            from handler in (
                from handler in validScopes
                from scope in handler.Scopes
                group handler by scope.Type
                into grouping
                orderby grouping.Key descending
                select grouping
            ).First()
            let validationResult = handler.Selector?.Invoke(update)
            where !validationResult.HasValue || validationResult.Value
            orderby validationResult descending
            select handler;

        return results.FirstOrDefault()?.Type;
    }


    private async Task<bool> IsCommand(Update update)
    {
        if (update.Type != UpdateType.Message)
        {
            return false;
        }

        if (update.Message is not { } message)
        {
            return false;
        }

        if (message.Type != MessageType.Text)
        {
            return false;
        }

        if (string.IsNullOrWhiteSpace(message.Text))
        {
            return false;
        }

        if (message.Entities?.First() is not { Type: MessageEntityType.BotCommand, Offset: 0 } entity)
        {
            return false;
        }

        string commandText = message.Text[entity.Offset..(entity.Offset + entity.Length)];

        (string commandName, string? botName) = PreprocessCommand(commandText);

        if (!await HasCorrectBotName(botName).ConfigureAwait(false))
        {
            return false;
        }

        if (!this.IsCommandName(commandName))
        {
            return false;
        }

        return true;
    }

    private static (string CommandName, string? BotName) PreprocessCommand(string input)
    {
        // ReSharper disable once PossiblyMistakenUseOfParamsMethod
        string[] commandNameData = input.Split('/', '@');
        return (commandNameData[1].ToLowerInvariant(), commandNameData.Length == 2 ? null : commandNameData[2]);
    }

    private async Task<bool> HasCorrectBotName(string? botName)
    {
        if (string.IsNullOrWhiteSpace(botName))
        {
            return true;
        }

        BotUser bot = await _botService.Get().ConfigureAwait(false);

        if (bot.Username.Equals(botName, StringComparison.InvariantCultureIgnoreCase))
        {
            return true;
        }

        return false;
    }
}