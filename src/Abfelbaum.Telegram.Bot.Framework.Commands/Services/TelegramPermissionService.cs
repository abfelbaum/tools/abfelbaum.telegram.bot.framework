using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Services;

public class TelegramPermissionService : ITelegramPermissionService
{
    private readonly ICachedTelegramBotClient _cachedClient;
    private readonly DefaultContext _context;

    public TelegramPermissionService(ICachedTelegramBotClient cachedClient,
        DefaultContext context)
    {
        _cachedClient = cachedClient;
        _context = context;
    }

    public async Task<bool> IsChatOwnerOrAdministrator(long userId, CancellationToken cancellationToken = new())
    {
        return await IsChatOwner(userId, cancellationToken).ConfigureAwait(false) ||
               await IsChatAdministrator(userId, cancellationToken).ConfigureAwait(false);
    }

    public async Task<bool> IsChatAdministrator(long userId, CancellationToken cancellationToken = new())
    {
        IEnumerable<ChatMember> administrators = await GetChatAdministrators(cancellationToken).ConfigureAwait(false);

        return administrators.Any(a => a.User.Id == userId);
    }

    public async Task<bool> IsChatOwner(long userId, CancellationToken cancellationToken = new())
    {
        IEnumerable<ChatMember> administrators = await GetChatAdministrators(cancellationToken).ConfigureAwait(false);

        return administrators.Any(a => a is ChatMemberOwner && (a.User.Id == userId));
    }

    private async Task<IEnumerable<ChatMember>> GetChatAdministrators(CancellationToken cancellationToken = new())
    {
        if (_context.Chat is null)
        {
            throw new ArgumentNullException(nameof(_context.Chat));
        }

        return await _cachedClient.MakeRequestAsync(new GetChatAdministratorsRequest
        {
            ChatId = _context.Chat.Id
        }, cancellationToken: cancellationToken).ConfigureAwait(false);
    }
}