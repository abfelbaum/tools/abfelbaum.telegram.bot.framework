using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Services;

public class CommandUpdaterService : IHostedService
{
    private readonly ITelegramBotClient _client;
    private readonly ILogger<CommandUpdaterService> _logger;
    private readonly IServiceScopeFactory _scopeFactory;

    public CommandUpdaterService(ITelegramBotClient client, ILogger<CommandUpdaterService> logger,
        IServiceScopeFactory scopeFactory)
    {
        _client = client;
        _logger = logger;
        _scopeFactory = scopeFactory;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Set telegram commands...");

        await using AsyncServiceScope serviceScope = _scopeFactory.CreateAsyncScope();
        IEnumerable<ITelegramCommand> commands =
            serviceScope.ServiceProvider.GetRequiredService<IEnumerable<ITelegramCommand>>();

        var scopes = new BotCommandScopeDictionary();
        foreach (ITelegramCommand command in commands)
        {
            foreach (CommandScope scope in command.Handlers.SelectMany(x => x.Scopes).Distinct())
            {
                scopes.Add(scope, command.ToBotCommand());

                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                CultureInfo currentUiCulture = CultureInfo.CurrentUICulture;
                foreach (CultureInfo cultureInfo in command.Documentation.CultureInfos)
                {
                    CultureInfo.CurrentCulture = cultureInfo;
                    CultureInfo.CurrentUICulture = cultureInfo;
                    scopes.Add(scope, command.ToBotCommand(cultureInfo), cultureInfo);
                }

                CultureInfo.CurrentCulture = currentCulture;
                CultureInfo.CurrentUICulture = currentUiCulture;
            }
        }

        foreach ((BotCommandScopeDictionaryKey key, IEnumerable<BotCommand> botCommands) in scopes)
        {
            await _client.MakeRequestAsync(new DeleteMyCommandsRequest
            {
                Scope = key.Scope.AsBotCommandScope(),
                LanguageCode = key.TwoLetterISOLanguageName,
            }, cancellationToken).ConfigureAwait(false);

            await _client.MakeRequestAsync(new SetMyCommandsRequest
            {
                Commands = botCommands,
                Scope = key.Scope.AsBotCommandScope(),
                LanguageCode = key.TwoLetterISOLanguageName
            }, cancellationToken: cancellationToken).ConfigureAwait(false);
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}