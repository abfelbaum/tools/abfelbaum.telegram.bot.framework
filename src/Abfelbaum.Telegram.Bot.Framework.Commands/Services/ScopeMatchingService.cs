using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Services;

public class ScopeMatchingService : IScopeMatchingService
{
    private readonly ITelegramPermissionService _permissions;

    public ScopeMatchingService(ITelegramPermissionService permissions)
    {
        _permissions = permissions;
    }

    public async Task<bool> ScopeContains(CommandScope scope, Chat chat, User? user,
        CancellationToken cancellationToken = new())
    {
        switch (scope.Type)
        {
            case BotCommandScopeType.Default:
                return true;

            case BotCommandScopeType.AllPrivateChats:
                return IsPrivateChat(chat.Type);

            case BotCommandScopeType.AllGroupChats:
                return IsGroupChat(chat.Type);

            case BotCommandScopeType.AllChatAdministrators:
                return await IsAnyGroupsChatAdministrator(chat.Type, user, cancellationToken);

            case BotCommandScopeType.Chat:
                return IsChat(chat.Id, scope.ChatId);

            case BotCommandScopeType.ChatAdministrators:
                return await IsGroupsChatAdministrator(chat.Id, scope.ChatId, user, cancellationToken);

            case BotCommandScopeType.ChatMember:
                return IsChatMember(chat.Id, scope.ChatId, user?.Id, scope.UserId);

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public async Task<bool> ScopesContain(IEnumerable<CommandScope> scopes, Chat chat, User? user,
        CancellationToken cancellationToken = new())
    {
        foreach (CommandScope scope in scopes)
        {
            if (!await ScopeContains(scope, chat, user, cancellationToken))
            {
                continue;
            }

            return true;
        }

        return false;
    }

    private bool IsChat(ChatId? chatId, ChatId? otherChatId)
    {
        return chatId is not null && chatId == otherChatId;
    }

    private bool IsUser(long? userId, long? otherUserId)
    {
        return userId is not null && userId is not 0 && userId == otherUserId;
    }

    private bool IsPrivateChat(ChatType chatType)
    {
        return chatType == ChatType.Private;
    }

    private bool IsGroupChat(ChatType chatType)
    {
        return chatType is ChatType.Group or ChatType.Supergroup;
    }

    private async Task<bool> IsAnyGroupsChatAdministrator(ChatType chatType, User? user, CancellationToken cancellationToken)
    {
        if (!IsGroupChat(chatType))
        {
            return false;
        }

        if (user is null)
        {
            return false;
        }

        return await IsChatOwnerOrAdministrator(user, cancellationToken);
    }

    private async Task<bool> IsGroupsChatAdministrator(ChatId? chatId, ChatId? otherChatId, User? user, CancellationToken cancellationToken)
    {
        if (!IsChat(chatId, otherChatId))
        {
            return false;
        }

        return await IsChatOwnerOrAdministrator(user, cancellationToken);
    }

    private async Task<bool> IsChatOwnerOrAdministrator(User? user, CancellationToken cancellationToken)
    {
        if (user is null)
        {
            return false;
        }

        return await _permissions.IsChatOwnerOrAdministrator(user.Id, cancellationToken).ConfigureAwait(false);
    }

    private bool IsChatMember(ChatId? chatId, ChatId? otherChatId, long? userId, long? otherUserId)
    {
        return IsChat(chatId, otherChatId) && IsUser(userId, otherUserId);
    }
}