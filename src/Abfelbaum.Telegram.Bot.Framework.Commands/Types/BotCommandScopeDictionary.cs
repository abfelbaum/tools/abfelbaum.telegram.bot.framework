using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public class
    BotCommandScopeDictionary : IEnumerable<KeyValuePair<BotCommandScopeDictionaryKey, IEnumerable<TelegramBotCommand>>>
{
    private readonly IDictionary<BotCommandScopeDictionaryKey, List<TelegramBotCommand>> _dictionary =
        new Dictionary<BotCommandScopeDictionaryKey, List<TelegramBotCommand>>();


    public IEnumerator<KeyValuePair<BotCommandScopeDictionaryKey, IEnumerable<TelegramBotCommand>>> GetEnumerator()
    {
        return _dictionary.Keys
            .Select(scope =>
                new KeyValuePair<BotCommandScopeDictionaryKey, IEnumerable<TelegramBotCommand>>(scope, Get(scope)))
            .GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Add(CommandScope scope, TelegramBotCommand command, CultureInfo? cultureInfo = null)
    {
        var key = new BotCommandScopeDictionaryKey(scope, cultureInfo);

        if (!_dictionary.ContainsKey(key))
        {
            _dictionary[key] = new List<TelegramBotCommand>();
        }

        _dictionary[key].Add(command);
    }

    private IEnumerable<TelegramBotCommand> Get(BotCommandScopeDictionaryKey key)
    {
        var result = new List<TelegramBotCommand>();
        foreach ((BotCommandScopeDictionaryKey commandsKey, List<TelegramBotCommand>? commands) in
                 _dictionary.OrderByDescending(x => x.Key.TwoLetterISOLanguageName))
        {
            if (commandsKey.Scope.Type > key.Scope.Type)
            {
                continue;
            }

            if (((int)commandsKey.Scope.Type > 4) && (commandsKey.Scope.ChatId! != key.Scope.ChatId!))
            {
                continue;
            }

            if (key.TwoLetterISOLanguageName != commandsKey.TwoLetterISOLanguageName &&
                commandsKey.TwoLetterISOLanguageName is not null)
            {
                continue;
            }

            foreach (TelegramBotCommand command in commands.Where(command =>
                         result.All(r => r.Command != command.Command && r.Id != command.Id)))
            {
                result.Add(command);
            }
        }

        return result;
    }
}