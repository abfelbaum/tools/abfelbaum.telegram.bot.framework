using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public class TelegramBotCommand : BotCommand
{
    public string? Id { get; init; }
}