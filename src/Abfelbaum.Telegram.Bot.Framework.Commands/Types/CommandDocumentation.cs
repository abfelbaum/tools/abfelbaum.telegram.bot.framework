using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.Extensions.Localization;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public class CommandDocumentation
{
    public string Id { get; } = Guid.NewGuid().ToString();
    public required string Name { get; init; }

    public string? ShortDescription { get; init; }

    public string? LongDescription { get; init; }

    public IList<string> Usages { get; } = [];
    public IList<string> Aliases { get; } = [];

    public IList<CultureInfo> CultureInfos { get; } = [];
    public IStringLocalizer? Localizer { get; init; }

    public string GetName(CultureInfo? cultureInfo = null) => Translate(Name, cultureInfo)!;
    public string GetShortDescription(CultureInfo? cultureInfo = null) => Translate(ShortDescription, cultureInfo)!;
    public string GetLongDescription(CultureInfo? cultureInfo = null) => Translate(LongDescription, cultureInfo)!;
    public IReadOnlyList<string> GetUsages(CultureInfo? cultureInfo = null) => Usages.Select(x => Translate(x, cultureInfo)!).ToList();
    public IReadOnlyList<string> GetAliases(CultureInfo? cultureInfo = null) => Aliases.Select(x => Translate(x, cultureInfo)!).ToList();



    private string? Translate(string? name, CultureInfo? cultureInfo = null)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            return null;
        }

        if (Localizer is null)
        {
            return name;
        }

        CultureInfo currentCulture = CultureInfo.CurrentCulture;
        CultureInfo currentUiCulture = CultureInfo.CurrentUICulture;
        if (cultureInfo is not null)
        {
            CultureInfo.CurrentCulture = cultureInfo;
            CultureInfo.CurrentUICulture = cultureInfo;
        }

        LocalizedString localizedString = Localizer[name];

        if (cultureInfo is not null)
        {
            CultureInfo.CurrentCulture = currentCulture;
            CultureInfo.CurrentUICulture = currentUiCulture;
        }

        return localizedString;
    }
}