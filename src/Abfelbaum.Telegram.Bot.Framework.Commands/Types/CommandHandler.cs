using System;
using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public class CommandHandler
{
    public CommandHandler(Type type, params CommandScope[] scopes)
    {
        Type = type ?? throw new ArgumentNullException(nameof(type));
        Scopes = scopes;
    }

    public Type Type { get; }
    public IEnumerable<CommandScope> Scopes { get; }
    public CommandHandlerSelector? Selector { get; init; }
}

public class CommandHandler<THandler> : CommandHandler where THandler : ITelegramUpdate<Message>
{
    public CommandHandler(params CommandScope[] scopes) : base(typeof(THandler), scopes)
    {
    }
}

public delegate bool CommandHandlerSelector(Update update);