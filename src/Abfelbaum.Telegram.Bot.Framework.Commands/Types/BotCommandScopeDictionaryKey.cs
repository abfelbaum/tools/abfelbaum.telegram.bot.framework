using System.Globalization;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public struct BotCommandScopeDictionaryKey
{
    public BotCommandScopeDictionaryKey(CommandScope scope, CultureInfo? cultureInfo = null)
    {
        Scope = scope;
        TwoLetterISOLanguageName = cultureInfo?.TwoLetterISOLanguageName;
    }

    public CommandScope Scope { get; }
    // ReSharper disable once InconsistentNaming
    public string? TwoLetterISOLanguageName { get; }
}