using System;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public class SimpleCommandConfiguration
{
    public CommandDocumentation Documentation { get; }
    public CommandScope[] Scopes { get; }
    public CommandHandlerSelector? Selector { get; init; }

    public SimpleCommandConfiguration(CommandDocumentation documentation, params CommandScope[] scopes)
    {
        Documentation = documentation ?? throw new ArgumentNullException(nameof(documentation));

        Scopes = scopes;
    }
}