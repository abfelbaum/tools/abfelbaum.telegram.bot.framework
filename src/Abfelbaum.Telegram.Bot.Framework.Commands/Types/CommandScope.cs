using System;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Types;

public record CommandScope
{
    public required BotCommandScopeType Type { get; init; }
    public ChatId? ChatId { get; init; }
    public long? UserId { get; init; }

    public static CommandScope Default()
    {
        return new CommandScope
        {
            Type = BotCommandScopeType.Default
        };
    }

    public static CommandScope AllPrivateChats()
    {
        return new CommandScope
        {
            Type = BotCommandScopeType.AllPrivateChats
        };
    }

    public static CommandScope AllGroupChats()
    {
        return new CommandScope
        {
            Type = BotCommandScopeType.AllGroupChats
        };
    }

    public static CommandScope AllChatAdministrators()
    {
        return new CommandScope
        {
            Type = BotCommandScopeType.AllChatAdministrators
        };
    }

    public static CommandScope Chat(ChatId chatId)
    {
        ArgumentNullException.ThrowIfNull(chatId);

        return new CommandScope
        {
            Type = BotCommandScopeType.Chat,
            ChatId = chatId
        };
    }

    public static CommandScope ChatAdministrators(ChatId chatId)
    {
        ArgumentNullException.ThrowIfNull(chatId);

        return new CommandScope
        {
            Type = BotCommandScopeType.ChatAdministrators,
            ChatId = chatId
        };
    }

    public static CommandScope ChatMember(ChatId chatId, long userId)
    {
        ArgumentNullException.ThrowIfNull(chatId);
        ArgumentNullException.ThrowIfNull(userId);

        return new CommandScope
        {
            Type = BotCommandScopeType.ChatMember,
            ChatId = chatId,
            UserId = userId
        };
    }
}