using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class SimpleCommandTelegramApplicationBuilderExtensions
{
    public static ITelegramApplicationBuilder AddSimpleCommand<THandler>(this ITelegramApplicationBuilder builder, SimpleCommandConfiguration configuration)
        where THandler : ITelegramUpdate<Message>
    {
        if (configuration is null)
        {
            throw new ArgumentNullException(nameof(configuration));
        }

        builder.AddCommand(SimpleCommandFactory);

        return builder;

        SimpleTelegramCommand<THandler> SimpleCommandFactory(IServiceProvider provider)
        {
            IBotService botService = provider.GetRequiredService<IBotService>();
            IScopeMatchingService scopeMatchingService = provider.GetRequiredService<IScopeMatchingService>();

            return new SimpleTelegramCommand<THandler>(botService, scopeMatchingService)
            {
                Documentation = configuration.Documentation,
                Scopes = configuration.Scopes,
                Selector = configuration.Selector
            };
        }
    }
}