using System;
using System.Globalization;
using System.Linq;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class TelegramBotCommandExtensions
{
    public static TelegramBotCommand ToBotCommand(this ITelegramCommand command, CultureInfo? cultureInfo = null) =>
        new()
        {
            Id = command.Documentation.Id,
            Command = command.Documentation.GetName(cultureInfo),
            Description = command.Documentation.GetShortDescription()
        };

    public static bool IsCommandName(this ITelegramCommand command, string? input)
    {
        bool result = command.IsCommandName(input, CultureInfo.CurrentUICulture);

        if (result)
        {
            return result;
        }

        return command.Documentation.CultureInfos.Any(cultureInfo => command.IsCommandName(input, cultureInfo));
    }

    public static bool IsCommandName(this ITelegramCommand command, string? input, CultureInfo cultureInfo)
    {
        if (input is null)
        {
            return false;
        }

        if (input.Equals(command.Documentation.GetName(cultureInfo), StringComparison.InvariantCultureIgnoreCase))
        {
            return true;
        }

        if (command.Documentation.GetAliases(cultureInfo).Any(x => input.Equals(x, StringComparison.InvariantCultureIgnoreCase)))
        {
            return true;
        }

        return false;
    }
}