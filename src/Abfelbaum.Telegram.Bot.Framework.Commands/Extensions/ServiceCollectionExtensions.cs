using Abfelbaum.Telegram.Bot.Framework.Commands.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCommandUpdater(this IServiceCollection services)
    {
        services.AddHostedService<CommandUpdaterService>();

        return services;
    }
}