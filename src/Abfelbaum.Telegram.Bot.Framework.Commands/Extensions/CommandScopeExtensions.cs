﻿using System;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class CommandScopeExtensions
{
    public static BotCommandScope AsBotCommandScope(this CommandScope scope)
    {
        ArgumentNullException.ThrowIfNull(scope);

        return scope.Type switch
        {
            BotCommandScopeType.Default => BotCommandScope.Default(),
            BotCommandScopeType.AllPrivateChats => BotCommandScope.AllPrivateChats(),
            BotCommandScopeType.AllGroupChats => BotCommandScope.AllGroupChats(),
            BotCommandScopeType.AllChatAdministrators => BotCommandScope.AllChatAdministrators(),
            BotCommandScopeType.Chat => BotCommandScope.Chat(scope.ChatId!),
            BotCommandScopeType.ChatAdministrators => BotCommandScope.ChatAdministrators(scope.ChatId!),
            BotCommandScopeType.ChatMember => BotCommandScope.ChatMember(scope.ChatId!, scope.UserId!.Value),
            _ => throw new ArgumentOutOfRangeException(nameof(scope))
        };
    }
}