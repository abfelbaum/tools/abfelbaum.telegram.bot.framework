using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class MessageExtensions
{
    public static bool TryGetCommandArguments(this Message message, [NotNullWhen(true)] out string? arguments)
    {
        if (message.Entities is null || message.Entities.Length == 0 || message.Text is null)
        {
            arguments = null;
            return false;
        }

        char[] chars = message.Text.Skip(message.Entities.First().Length).ToArray();

        arguments = new string(chars).Trim();

        if (arguments.Length == 0)
        {
            arguments = null;
            return false;
        }

        return true;
    }
}