using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Services;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;

public static class TelegramApplicationBuilderExtensions
{
    public static ITelegramApplicationBuilder AddCommand<TTelegramCommand>(this ITelegramApplicationBuilder builder)
        where TTelegramCommand : TelegramUpdate, ITelegramCommand
    {
        builder.AddUpdate<TTelegramCommand>(UpdateType.Message);
        builder.Services.AddTransient<ITelegramCommand, TTelegramCommand>();
        builder.Services.TryAddTransient<IScopeMatchingService, ScopeMatchingService>();
        builder.Services.TryAddTransient<ITelegramPermissionService, TelegramPermissionService>();
        builder.AddCachedClient();

        return builder;
    }

    public static ITelegramApplicationBuilder AddCommand<TTelegramCommand>(this ITelegramApplicationBuilder builder, Func<IServiceProvider, TTelegramCommand> factory)
        where TTelegramCommand : TelegramUpdate, ITelegramCommand
    {
        builder.AddUpdate(factory, UpdateType.Message);
        builder.Services.AddTransient<ITelegramCommand, TTelegramCommand>(factory);
        builder.Services.TryAddTransient<IScopeMatchingService, ScopeMatchingService>();
        builder.Services.TryAddTransient<ITelegramPermissionService, TelegramPermissionService>();
        builder.AddCachedClient();

        return builder;
    }
}