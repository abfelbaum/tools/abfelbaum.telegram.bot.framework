using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace Abfelbaum.Telegram.Bot.Framework.RateLimiting;

public static class ServiceCollectionExtensions
{
    public static OptionsBuilder<SchedulerOptions> AddRequestScheduler(this IServiceCollection services)
    {
        services.TryAddSingleton<IRequestScheduler, RequestScheduler>();

        services.AddHostedService<RequestSchedulerBackgroundService>();

        return services.AddOptions<SchedulerOptions>();
    }

    public static void AddRequestScheduler(this IServiceCollection services, IConfiguration configuration)
    {
        OptionsBuilder<SchedulerOptions> optionsBuilder = services.AddRequestScheduler();

        optionsBuilder.Bind(configuration);
    }

    public static void AddRequestScheduler(this IServiceCollection services, Action<SchedulerOptions> configureOptions)
    {
        OptionsBuilder<SchedulerOptions> optionsBuilder = services.AddRequestScheduler();

        optionsBuilder.Configure(configureOptions);
    }
}