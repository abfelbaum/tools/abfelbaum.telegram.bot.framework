// Copyright (c) Miha Zupan. All rights reserved.
// This file is licensed under the MIT license.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SharpCollections.Generic;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.RateLimiting;

public class RequestScheduler : IRequestScheduler, IDisposable, IAsyncDisposable
{
    private readonly Dictionary<long, Queue<TaskCompletionSource<bool>>> _buckets;
    private readonly long _generalTimestampIncrement;
    private readonly long _groupTimestampIncrement;

    private readonly object _lock;
    private readonly SchedulerOptions _options;
    private readonly long _privateTimestampIncrement;
    private readonly BinaryHeap<RequestNode> _requestHeap;
    private readonly Timer _timer;
    private readonly long _timestampResetThreshold;

    private long _lastTimestamp;

    public RequestScheduler(IOptions<SchedulerOptions> options)
    {
        _options = options.Value;

        _generalTimestampIncrement = _options.SafeGeneralInterval * 10_000L;
        _privateTimestampIncrement = _options.SafePrivateChatInterval * 10_000L;
        _groupTimestampIncrement = _options.SafeGroupChatInterval * 10_000L;

        _timestampResetThreshold = _generalTimestampIncrement * 30;

        _lock = new object();
        _requestHeap = new BinaryHeap<RequestNode>(16);
        _buckets = new Dictionary<long, Queue<TaskCompletionSource<bool>>>(16);

        _lastTimestamp = 0;
#pragma warning disable CS8600
#pragma warning disable CS8602
        _timer = new Timer(s => ((RequestScheduler)s).OnInterval(), this, 0, Timeout.Infinite);
#pragma warning restore CS8602
#pragma warning restore CS8600
    }

#if DEBUG
    /// <summary>
    ///     For testing purposes only
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public int DEBUG_CURRENT_COUNT => _requestHeap.Count + _buckets.Count;
#endif


    public async ValueTask DisposeAsync()
    {
        try
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            await _timer.DisposeAsync().ConfigureAwait(false);
        }
        catch
        {
            // ignored
        }
    }

    public void Dispose()
    {
        try
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            _timer.Dispose();
        }
        catch
        {
            // ignored
        }
    }

    public Task YieldAsync(ChatId chatId)
    {
        if (chatId.Username is { } username)
        {
            return YieldAsyncCore(username.GetHashCode(), _groupTimestampIncrement);
        }

        return YieldAsync(chatId.Identifier ?? 0);
    }

    public Task YieldAsync(long bucket = 0)
    {
        long timestampIncrement = bucket == 0 ? _generalTimestampIncrement
            : bucket > 0 ? _privateTimestampIncrement
            : _groupTimestampIncrement;

        return YieldAsyncCore(bucket, timestampIncrement);
    }

    private long Timestamp()
    {
        return DateTime.UtcNow.Ticks;
    }

    // ReSharper disable once CognitiveComplexity
    private void OnInterval()
    {
        long timestamp = Timestamp();

        lock (_lock)
        {
            while (_lastTimestamp < timestamp)
            {
                // If the timing is waaaay off, reset instead of spinning the thread
                if ((timestamp - _lastTimestamp) > _timestampResetThreshold)
                {
                    _lastTimestamp = timestamp;
                }

                _lastTimestamp += _generalTimestampIncrement;

                while (!_requestHeap.IsEmpty && (_requestHeap.Top.Timestamp <= timestamp))
                {
                    RequestNode request = _requestHeap.Pop();

                    Queue<TaskCompletionSource<bool>> queue = _buckets[request.Bucket];

                    if (queue.Count == 0)
                    {
                        _buckets.Remove(request.Bucket);
                    }
                    else
                    {
                        queue.Dequeue().SetResult(false);
                        timestamp = Timestamp();
                        _requestHeap.Push(request.Next(timestamp));
                        break;
                    }
                }
            }
        }

        _timer.Change(_options.SafeGeneralInterval, Timeout.Infinite);
    }

    private Task YieldAsyncCore(long bucket, long timestampIncrement)
    {
        var tcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);

        lock (_lock)
        {
#pragma warning disable CS8600
            if (_buckets.TryGetValue(bucket, out Queue<TaskCompletionSource<bool>> queue))
#pragma warning restore CS8600
            {
                queue.Enqueue(tcs);
            }
            else
            {
                _buckets[bucket] = queue = new Queue<TaskCompletionSource<bool>>(4);
                queue.Enqueue(tcs);
                _requestHeap.Push(new RequestNode(bucket, Timestamp(), timestampIncrement));
            }
        }

        return tcs.Task;
    }

    private readonly struct RequestNode : IComparable<RequestNode>
    {
        public readonly long Bucket;
        public readonly long Timestamp;
        public readonly long TimestampIncrement;

        public RequestNode(long bucket, long timestamp, long timestampIncrement)
        {
            Bucket = bucket;
            Timestamp = timestamp;
            TimestampIncrement = timestampIncrement;
        }

        public RequestNode Next(long currentTimestamp)
        {
            return new RequestNode(Bucket, currentTimestamp + TimestampIncrement, TimestampIncrement);
        }

        public int CompareTo(RequestNode other)
        {
            return Timestamp.CompareTo(other.Timestamp);
        }
    }
}