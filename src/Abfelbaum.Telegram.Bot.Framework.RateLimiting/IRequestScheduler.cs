using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.RateLimiting;

public interface IRequestScheduler
{
    public Task YieldAsync(ChatId chatId);
    public Task YieldAsync(long bucket = 0);
}