using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Requests.Abstractions;

namespace Abfelbaum.Telegram.Bot.Framework.RateLimiting;

public class RequestSchedulerBackgroundService : IHostedService
{
    private readonly ITelegramBotClient _client;
    private readonly IRequestScheduler _scheduler;

    public RequestSchedulerBackgroundService(IRequestScheduler scheduler, ITelegramBotClient client)
    {
        _scheduler = scheduler;
        _client = client;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _client.OnMakingApiRequest += ClientOnOnMakingApiRequest;
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _client.OnMakingApiRequest -= ClientOnOnMakingApiRequest;

        return Task.CompletedTask;
    }

    private async ValueTask ClientOnOnMakingApiRequest(ITelegramBotClient botClient, ApiRequestEventArgs request,
        CancellationToken cancellationToken)
    {

        switch (request.Request)
        {
            case IChatTargetable chatTarget:
                await _scheduler.YieldAsync(chatTarget.ChatId).ConfigureAwait(false);
                return;
            case IUserTargetable userTarget:
                await _scheduler.YieldAsync(userTarget.UserId).ConfigureAwait(false);
                return;
            default:
                await _scheduler.YieldAsync().ConfigureAwait(false);
                break;
        }
    }
}