namespace Abfelbaum.Telegram.Bot.Framework.RateLimiting;

public class SchedulerOptions
{
    public int SafeGeneralInterval { get; set; } = 34;
    public int SafePrivateChatInterval { get; set; } = 1000;
    public int SafeGroupChatInterval { get; set; } = 3000;
}