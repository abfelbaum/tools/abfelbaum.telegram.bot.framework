using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddSequentialInputs(this IServiceCollection services)
    {
        services.TryAddSingleton<ISequentialInputService, SequentialInputService>();

        return services;
    }
}