using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Middlewares;
using Abfelbaum.Telegram.Bot.Framework.Types;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;

public static class ApplicationBuilderExtensions
{
    public static IApplicationBuilder<TContext> UseSequentialInputs<TContext>(this IApplicationBuilder<TContext> app)
        where TContext : DefaultContext
    {
        app.Use<SequentialInputMiddleware<TContext>>();

        return app;
    }
}