using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;

public static class SequentialInputServiceExtensions
{
    /// <summary>
    ///     Waits for a message that matches the specified conditions.
    /// </summary>
    /// <param name="inputService">The sequential input service</param>
    /// <param name="chatId">The chat id where the bot should listen</param>
    /// <param name="messageType">The type of message the bot should listen for</param>
    /// <param name="result">The result data</param>
    /// <param name="timeout">Timeout of the sequential input</param>
    /// <param name="userId">If personal, the id of the user</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <typeparam name="TResult">The result of result</typeparam>
    /// <returns>TResult</returns>
    public static Task<TResult> WaitForMessage<TResult>(this ISequentialInputService inputService, ChatId chatId,
        MessageType messageType, Func<Message, TResult> result, TimeSpan timeout, long? userId = null,
        CancellationToken cancellationToken = new())
    {
        return inputService.WaitForMessage(chatId, new[] { messageType }, result, timeout, userId, cancellationToken);
    }

    /// <summary>
    ///     Waits for a message that matches the specified conditions.
    /// </summary>
    /// <param name="inputService">The sequential input service</param>
    /// <param name="chatId">The chat id where the bot should listen</param>
    /// <param name="messageTypes">The types of message the bot should listen for</param>
    /// <param name="result">The result data</param>
    /// <param name="timeout">Timeout of the sequential input</param>
    /// <param name="userId">If personal, the id of the user</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <typeparam name="TResult">The result of result</typeparam>
    /// <returns>TResult</returns>
    public static Task<TResult> WaitForMessage<TResult>(this ISequentialInputService inputService, ChatId chatId,
        IEnumerable<MessageType> messageTypes, Func<Message, TResult> result, TimeSpan timeout, long? userId = null,
        CancellationToken cancellationToken = new())
    {
        return inputService.WaitFor(
            update =>
            {
                if (update.Type != UpdateType.Message)
                {
                    return false;
                }

                Message message = update.Message!;

                if (message.Chat.Id != chatId)
                {
                    return false;
                }

                if (userId is not null && (message.From?.Id != userId))
                {
                    return false;
                }

                if (messageTypes.All(x => x != message.Type))
                {
                    return false;
                }

                return true;
            },
            update => result(update.Message!),
            timeout, cancellationToken: cancellationToken);
    }

    /// <summary>
    ///     Waits for a callback that matches the specified conditions.
    ///     Do not forget to send an answerCallbackQuery request!
    /// </summary>
    /// <param name="inputService">The sequential input service</param>
    /// <param name="callbackId">An id the callback data should start with</param>
    /// <param name="result">The result you want from your callback</param>
    /// <param name="chatId">Id of the chat if provided</param>
    /// <param name="timeout">Timeout of the sequential input</param>
    /// <param name="userId">Id of the user, null if it is not personalized</param>
    /// <param name="cancellationToken">Cancellation Token</param>
    /// <typeparam name="TResult">The result of result</typeparam>
    /// <returns>TResult</returns>
    public static Task<TResult> WaitForCallback<TResult>(this ISequentialInputService inputService, string callbackId,
        Func<CallbackQuery, TResult> result, ChatId chatId, TimeSpan timeout, long? userId = null,
        CancellationToken cancellationToken = new())
    {
        return inputService.WaitFor(update =>
            {
                if (update.Type != UpdateType.CallbackQuery)
                {
                    return false;
                }

                CallbackQuery query = update.CallbackQuery!;

                if (query.Message is Message message && (chatId != message.Chat.Id))
                {
                    return false;
                }

                if (userId is not null && (query.From.Id != userId))
                {
                    return false;
                }

                if (query.Data is not null && !query.Data.StartsWith(callbackId))
                {
                    return false;
                }

                return true;
            },
            update => result(update.CallbackQuery!),
            timeout, cancellationToken: cancellationToken);
    }

    /// <summary>
    /// Waits for an update without returning anything
    /// </summary>
    /// <param name="inputService">SequentialInputService</param>
    /// <param name="condition">The condition that should be met</param>
    /// <param name="timeout">Timeout of the sequential input</param>
    /// <param name="stopPipeline">If the pipeline should stop after the input was recognized</param>
    /// <param name="cancellationToken">CancellationToken</param>
    /// <returns>Task</returns>
    public static Task WaitFor(this ISequentialInputService inputService, Func<Update, bool> condition,
        TimeSpan timeout, bool stopPipeline = false, CancellationToken cancellationToken = new()) =>
        inputService.WaitFor(condition, _ => true, timeout, stopPipeline, cancellationToken);
}