using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Middlewares;

public class SequentialInputMiddleware<TContext> : ITelegramMiddleware<TContext>
    where TContext : DefaultContext
{
    private readonly ISequentialInputService _sequentialInputService;

    public SequentialInputMiddleware(ISequentialInputService sequentialInputService)
    {
        _sequentialInputService = sequentialInputService;
    }

    public async Task InvokeAsync(TContext context, NextMiddleware next,
        CancellationToken cancellationToken = new())
    {
        if (!_sequentialInputService.InvokeSequentialInputs(context.Update))
        {
            await next().ConfigureAwait(false);
            return;
        }

        await context.RequestEnded().ConfigureAwait(false);
    }
}