using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Models;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Services;

public class SequentialInputService : ISequentialInputService
{
    private readonly ILogger<SequentialInputService> _logger;
    private List<SequentialInput> _inputs = new();

    private readonly List<Action<SequentialInput>> _cleanupConditions = new()
    {
        // Remove cancelled inputs
        input =>
        {
            if (input.CancellationToken.IsCancellationRequested)
            {
                input.TaskCompletionSource.TrySetCanceled();
            }
        }
    };

    public SequentialInputService(ILogger<SequentialInputService> logger)
    {
        _logger = logger;
    }

    public async Task<TResult> WaitFor<TResult>(Func<Update, bool> condition, Func<Update, TResult> result,
        TimeSpan timeout, bool stopPipeline = true,
        CancellationToken cancellationToken = new())
    {
        var taskSource = new TaskCompletionSource<object>();

        _inputs.Add(new SequentialInput
        {
            Condition = condition,
            Result = update => result(update)!,
            CancellationToken = cancellationToken,
            TaskCompletionSource = taskSource,
            Timeout = DateTime.UtcNow.Add(timeout),
            Stop = stopPipeline
        });

        _ = Task.Run(async () =>
        {
            await Task.Delay(timeout, cancellationToken).ConfigureAwait(false);
            taskSource.SetCanceled(cancellationToken);
        }, cancellationToken);

        object resultObj = await taskSource.Task.ConfigureAwait(false);
        return (TResult)resultObj;
    }

    public bool InvokeSequentialInputs(Update update)
    {
        Cleanup();

        List<SequentialInput> executableInputs = ValidateInputs(update, out List<SequentialInput> disposableInputs);

        disposableInputs = ExecuteInputs(update, executableInputs);

        RemoveInputs(disposableInputs, false);

        return disposableInputs.Any(x => x.Stop);
    }

    public void Cleanup()
    {
        _inputs = _inputs.Where(input => _cleanupConditions.All(condition =>
        {
            condition(input);

            Task<object> task = input.TaskCompletionSource.Task;

            return !task.IsCompleted;
        })).ToList();
    }

    private List<SequentialInput> ExecuteInputs(Update update, List<SequentialInput> executableInputs)
    {
        var disposableInputs = new List<SequentialInput>();

        foreach (SequentialInput input in executableInputs)
        {
            try
            {
                object result = input.Result(update);

                input.TaskCompletionSource.SetResult(result);
                disposableInputs.Add(input);
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "An error occurred while getting sequential input result: {Message}", e.Message);
                disposableInputs.Add(input);
            }
        }

        return disposableInputs;
    }

    private void RemoveInputs(List<SequentialInput> inputs, bool clear)
    {
        foreach (SequentialInput input in inputs)
        {
            _inputs.Remove(input);
        }

        if (clear)
        {
            inputs.Clear();
        }
    }

    private List<SequentialInput> ValidateInputs(Update update, out List<SequentialInput> disposableInputs)
    {
        var executableInputs = new List<SequentialInput>();
        disposableInputs = new List<SequentialInput>();
        foreach (SequentialInput input in _inputs)
        {
            try
            {
                if (!input.Condition(update))
                {
                    continue;
                }

                executableInputs.Add(input);
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "An error occurred while validating sequential inputs: {Message}", e.Message);
                disposableInputs.Add(input);
            }
        }

        return executableInputs;
    }
}