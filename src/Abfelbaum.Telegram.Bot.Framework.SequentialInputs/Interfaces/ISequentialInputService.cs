using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;

public interface ISequentialInputService
{
    /// <summary>
    ///     Waits for an update that meets the given conditions
    /// </summary>
    /// <param name="condition">The update condition that should be met</param>
    /// <param name="result">The result (type) you want</param>
    /// <param name="timeout">Timeout of the sequential input</param>
    /// <param name="stopPipeline">Defines if the update pipeline should continue when the result matched</param>
    /// <param name="cancellationToken">Cancellation token</param>
    /// <typeparam name="TResult">The result type of result</typeparam>
    /// <returns>A task that will wait until InvokeSequentialInputs provides an update that meets the given conditions</returns>
    public Task<TResult> WaitFor<TResult>(Func<Update, bool> condition, Func<Update, TResult> result, TimeSpan timeout,
        bool stopPipeline = true, CancellationToken cancellationToken = new());

    /// <summary>
    ///     Looks if the update meets any sequential inputs conditions and if they do, executes them.
    /// </summary>
    /// <param name="update">Telegram Update</param>
    /// <returns>True if any sequential input matched the update</returns>
    public bool InvokeSequentialInputs(Update update);

    public void Cleanup();
}