using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Models;

public class SequentialInput
{
    public Func<Update, bool> Condition { get; init; } = null!;
    public Func<Update, object> Result { get; init; } = null!;
    public CancellationToken CancellationToken { get; init; }
    public TaskCompletionSource<object> TaskCompletionSource { get; init; } = null!;
    public DateTime Timeout { get; init; }
    public bool Stop { get; init; }
}