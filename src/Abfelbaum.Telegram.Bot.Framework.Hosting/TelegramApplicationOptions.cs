// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Abfelbaum.Telegram.Bot.Framework.Hosting;

/// <summary>
///     Options for configuring the behavior for <see cref="TelegramApplicationBuilder{TContext}" />.
/// </summary>
public class TelegramApplicationOptions
{
    /// <summary>
    ///     The command line arguments.
    /// </summary>
    public string[]? Args { get; init; }

    /// <summary>
    ///     The environment name.
    /// </summary>
    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    public string? EnvironmentName { get; init; }

    /// <summary>
    ///     The application name.
    /// </summary>
    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    public string? ApplicationName { get; init; }

    internal void ApplyHostConfiguration(IConfigurationBuilder builder)
    {
        Dictionary<string, string>? config = null;

        if (EnvironmentName is not null)
        {
            config = new Dictionary<string, string>
            {
                [HostDefaults.EnvironmentKey] = EnvironmentName
            };
        }

        if (ApplicationName is not null)
        {
            config ??= new Dictionary<string, string>();
            config[HostDefaults.ApplicationKey] = ApplicationName;
        }

        if (config is not null)
        {
            builder.AddInMemoryCollection(config!);
        }
    }
}