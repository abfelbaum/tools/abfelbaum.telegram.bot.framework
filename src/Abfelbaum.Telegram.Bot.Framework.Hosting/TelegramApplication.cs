// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using System;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.Hosting;

/// <summary>
///     The web application used to configure the HTTP pipeline, and routes.
/// </summary>
public sealed class TelegramApplication<TContext> : IHost, IApplicationBuilder<TContext>, IAsyncDisposable
    where TContext : DefaultContext
{
    private readonly IHost _host;
    private readonly IPipelineBuilder<TContext> _pipelineBuilder;

    internal TelegramApplication(IHost host, ref IPipelineBuilder<TContext> pipelineBuilder)
    {
        _host = host;
        _pipelineBuilder = pipelineBuilder;
    }

    /// <summary>
    ///     The application's configured <see cref="IConfiguration" />.
    /// </summary>
    public IConfiguration Configuration => _host.Services.GetRequiredService<IConfiguration>();

    /// <summary>
    ///     The application's configured <see cref="IHostEnvironment" />.
    /// </summary>
    public IHostEnvironment Environment => _host.Services.GetRequiredService<IHostEnvironment>();

    /// <summary>
    ///     Allows consumers to be notified of application lifetime events.
    /// </summary>
    public IHostApplicationLifetime Lifetime => _host.Services.GetRequiredService<IHostApplicationLifetime>();

    /// <summary>
    ///     Allows consumers to register required updates for Telegram
    /// </summary>
    public IRequiredUpdates RequiredUpdates => _host.Services.GetRequiredService<IRequiredUpdates>();

    /// <summary>
    ///     The default logger for the application.
    /// </summary>
    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    public ILogger Logger =>
        _host.Services.GetRequiredService<ILoggerFactory>().CreateLogger(Environment.ApplicationName);

    /// <summary>
    ///     Disposes the application.
    /// </summary>
    public ValueTask DisposeAsync()
    {
        return ((IAsyncDisposable)_host).DisposeAsync();
    }

    /// <summary>
    ///     The application's configured services.
    /// </summary>
    public IServiceProvider Services => _host.Services;

    /// <summary>
    ///     Start the application.
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns>
    ///     A <see cref="Task" /> that represents the startup of the <see cref="TelegramApplicationBuilder{TContext}" />.
    ///     Successful completion indicates the HTTP server is ready to accept new requests.
    /// </returns>
    public Task StartAsync(CancellationToken cancellationToken = default)
    {
        return _host.StartAsync(cancellationToken);
    }

    /// <summary>
    ///     Shuts down the application.
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns>
    ///     A <see cref="Task" /> that represents the shutdown of the <see cref="TelegramApplicationBuilder{TContext}" />.
    ///     Successful completion indicates that all the HTTP server has stopped.
    /// </returns>
    public Task StopAsync(CancellationToken cancellationToken = default)
    {
        return _host.StopAsync(cancellationToken);
    }

    /// <summary>
    ///     Disposes the application.
    /// </summary>
    void IDisposable.Dispose()
    {
        _host.Dispose();
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="TelegramApplication{TContext}" /> class with preconfigured defaults.
    /// </summary>
    /// <param name="args">Command line arguments</param>
    /// <returns>The <see cref="TelegramApplication{TContext}" />.</returns>
    public static TelegramApplication<TContext> Create(string[]? args = null)
    {
        return new TelegramApplicationBuilder<TContext>(new TelegramApplicationOptions { Args = args }).Build();
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="TelegramApplicationBuilder{TContext}" /> class with preconfigured
    ///     defaults.
    /// </summary>
    /// <returns>The <see cref="TelegramApplicationBuilder{TContext}" />.</returns>
    public static TelegramApplicationBuilder<TContext> CreateBuilder()
    {
        return new TelegramApplicationBuilder<TContext>(new TelegramApplicationOptions());
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="TelegramApplicationBuilder{TContext}" /> class with preconfigured
    ///     defaults.
    /// </summary>
    /// <param name="args">Command line arguments</param>
    /// <returns>The <see cref="TelegramApplicationBuilder{TContext}" />.</returns>
    public static TelegramApplicationBuilder<TContext> CreateBuilder(string[] args)
    {
        return new TelegramApplicationBuilder<TContext>(new TelegramApplicationOptions { Args = args });
    }

    /// <summary>
    ///     Initializes a new instance of the <see cref="TelegramApplicationBuilder{TContext}" /> class with preconfigured
    ///     defaults.
    /// </summary>
    /// <param name="options">
    ///     The <see cref="TelegramApplicationOptions" /> to configure the
    ///     <see cref="TelegramApplicationBuilder{TContext}" />.
    /// </param>
    /// <returns>The <see cref="TelegramApplicationBuilder{TContext}" />.</returns>
    public static TelegramApplicationBuilder<TContext> CreateBuilder(TelegramApplicationOptions options)
    {
        return new TelegramApplicationBuilder<TContext>(options);
    }

    /// <summary>
    ///     Runs an application and returns a Task that only completes when the token is triggered or shutdown is triggered.
    /// </summary>
    /// <returns>
    ///     A <see cref="Task" /> that represents the entire runtime of the <see cref="TelegramApplicationBuilder{TContext}" />
    ///     from startup to shutdown.
    /// </returns>
    public Task RunAsync()
    {
        return HostingAbstractionsHostExtensions.RunAsync(this);
    }

    /// <summary>
    ///     Runs an application and block the calling thread until host shutdown.
    /// </summary>
    public void Run()
    {
        HostingAbstractionsHostExtensions.Run(this);
    }

    public IPipelineBuilder<TContext> Use<TMiddleware>() where TMiddleware : IMiddleware<TContext>
    {
        return _pipelineBuilder.Use<TMiddleware>();
    }

    public IPipelineBuilder<TContext> Use(Type middlewareType)
    {
        return _pipelineBuilder.Use(middlewareType);
    }

    public IPipelineBuilder<TContext> Use(IMiddleware<TContext> middleware)
    {
        return _pipelineBuilder.Use(middleware);
    }

    public IPipelineBuilder<TContext> Use(ParameterAsNextMiddlewareFactoryDelegate<TContext> middlewareFactory)
    {
        return _pipelineBuilder.Use(middlewareFactory);
    }

    public IPipelineBuilder<TContext> Use(ParameterWithServiceProviderAsNextMiddlewareFactoryDelegate<TContext> middlewareFactory)
    {
        return _pipelineBuilder.Use(middlewareFactory);
    }

    public IPipelineBuilder<TContext> Use(FuncAsNextMiddlewareDelegateWithServiceProvider<TContext> middlewareDelegateWithServiceProvider)
    {
        return _pipelineBuilder.Use(middlewareDelegateWithServiceProvider);
    }

    public IPipelineBuilder<TContext> Use(FuncAsNextMiddlewareDelegate<TContext> middlewareDelegate)
    {
        return _pipelineBuilder.Use(middlewareDelegate);
    }
}