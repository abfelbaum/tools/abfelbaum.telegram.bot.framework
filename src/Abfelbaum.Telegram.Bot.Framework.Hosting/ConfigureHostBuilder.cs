// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using System;
using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Abfelbaum.Telegram.Bot.Framework.Hosting;

/// <summary>
///     A non-buildable <see cref="IHostBuilder" /> for <see cref="TelegramApplicationBuilder{TContext}" />.
///     Use <see cref="TelegramApplicationBuilder{TContext}.Build" /> to build the
///     <see cref="TelegramApplicationBuilder{TContext}" />.
/// </summary>
public sealed class ConfigureHostBuilder : IHostBuilder
{
    private readonly ConfigurationManager _configuration;
    private readonly HostBuilderContext _context;

    private readonly List<Action<IHostBuilder>> _operations = new();
    private readonly IServiceCollection _services;

    internal ConfigureHostBuilder(HostBuilderContext context, ConfigurationManager configuration,
        IServiceCollection services)
    {
        _configuration = configuration;
        _services = services;
        _context = context;
    }

    /// <inheritdoc />
    public IDictionary<object, object> Properties => _context.Properties;

    IHost IHostBuilder.Build()
    {
        throw new NotSupportedException(
            $"Call {nameof(TelegramApplicationBuilder<DefaultContext>)}.{nameof(TelegramApplicationBuilder<DefaultContext>.Build)}() instead.");
    }

    /// <inheritdoc />
    public IHostBuilder ConfigureAppConfiguration(Action<HostBuilderContext, IConfigurationBuilder> configureDelegate)
    {
        // Run these immediately so that they are observable by the imperative code
        configureDelegate(_context, _configuration);
        return this;
    }

    /// <inheritdoc />
    public IHostBuilder ConfigureContainer<TContainerBuilder>(
        Action<HostBuilderContext, TContainerBuilder> configureDelegate)
    {
        if (configureDelegate is null)
        {
            throw new ArgumentNullException(nameof(configureDelegate));
        }

        _operations.Add(b => b.ConfigureContainer(configureDelegate));
        return this;
    }

    /// <inheritdoc />
    public IHostBuilder ConfigureHostConfiguration(Action<IConfigurationBuilder> configureDelegate)
    {
        string? previousApplicationName = _configuration[HostDefaults.ApplicationKey];
        string? previousContentRoot = _configuration[HostDefaults.ContentRootKey];
        string? previousEnvironment = _configuration[HostDefaults.EnvironmentKey];

        // Run these immediately so that they are observable by the imperative code
        configureDelegate(_configuration);

        // Disallow changing any host settings this late in the cycle, the reasoning is that we've already loaded the default configuration
        // and done other things based on environment name, application name or content root.
        if (!string.Equals(previousApplicationName, _configuration[HostDefaults.ApplicationKey],
                StringComparison.OrdinalIgnoreCase))
        {
            throw new NotSupportedException(
                "The application name changed. Changing the host configuration is not supported");
        }

        if (!string.Equals(previousContentRoot, _configuration[HostDefaults.ContentRootKey],
                StringComparison.OrdinalIgnoreCase))
        {
            throw new NotSupportedException(
                "The content root changed. Changing the host configuration is not supported");
        }

        if (!string.Equals(previousEnvironment, _configuration[HostDefaults.EnvironmentKey],
                StringComparison.OrdinalIgnoreCase))
        {
            throw new NotSupportedException(
                "The environment changed. Changing the host configuration is not supported");
        }

        return this;
    }

    /// <inheritdoc />
    public IHostBuilder ConfigureServices(Action<HostBuilderContext, IServiceCollection> configureDelegate)
    {
        // Run these immediately so that they are observable by the imperative code
        configureDelegate(_context, _services);
        return this;
    }

    /// <inheritdoc />
    public IHostBuilder UseServiceProviderFactory<TContainerBuilder>(IServiceProviderFactory<TContainerBuilder> factory)
        where TContainerBuilder : notnull
    {
        if (factory is null)
        {
            throw new ArgumentNullException(nameof(factory));
        }

        _operations.Add(b => b.UseServiceProviderFactory(factory));
        return this;
    }

    /// <inheritdoc />
    public IHostBuilder UseServiceProviderFactory<TContainerBuilder>(
        Func<HostBuilderContext, IServiceProviderFactory<TContainerBuilder>> factory) where TContainerBuilder : notnull
    {
        if (factory is null)
        {
            throw new ArgumentNullException(nameof(factory));
        }

        _operations.Add(b => b.UseServiceProviderFactory(factory));
        return this;
    }

    internal void RunDeferredCallbacks(IHostBuilder hostBuilder)
    {
        foreach (Action<IHostBuilder> operation in _operations)
        {
            operation(hostBuilder);
        }
    }
}