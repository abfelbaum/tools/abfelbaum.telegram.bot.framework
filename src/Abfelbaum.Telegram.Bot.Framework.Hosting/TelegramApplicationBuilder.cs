// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using System;
using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Services;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Middlewares;

// ReSharper disable UnusedAutoPropertyAccessor.Global
namespace Abfelbaum.Telegram.Bot.Framework.Hosting;

/// <summary>
///     A builder for web applications and services.
/// </summary>
public sealed class TelegramApplicationBuilder<TContext> : ITelegramApplicationBuilder where TContext : DefaultContext
{
    private readonly HostBuilder _hostBuilder = new();
    private readonly List<KeyValuePair<string, string>> _hostConfigurationValues;
    private readonly TelegramApplicationServiceCollection _services = new();

    private TelegramApplication<TContext>? _builtApplication;
    private IPipelineBuilder<TContext>? _pipelineBuilder;

    internal TelegramApplicationBuilder(TelegramApplicationOptions options,
        Action<IHostBuilder>? configureDefaults = null)
    {
        Services = _services;

        string[]? args = options.Args;

        // Run methods to configure both generic and web host defaults early to populate config from appsettings.json
        // environment variables (both DOTNET_ and ASPNETCORE_ prefixed) and other possible default sources to prepopulate
        // the correct defaults.
        var bootstrapHostBuilder = new BootstrapHostBuilder(Services, _hostBuilder.Properties);

        // Don't specify the args here since we want to apply them later so that args
        // can override the defaults specified by ConfigureWebHostDefaults
        bootstrapHostBuilder.ConfigureDefaults(args: null);

        // This is for testing purposes
        configureDefaults?.Invoke(bootstrapHostBuilder);

        // We specify the command line here last since we skipped the one in the call to ConfigureDefaults.
        // The args can contain both host and application settings so we want to make sure
        // we order those configuration providers appropriately without duplicating them
        if (args is { Length: > 0 })
        {
            bootstrapHostBuilder.ConfigureAppConfiguration(config => { config.AddCommandLine(args); });
        }

        // Apply the args to host configuration last since ConfigureWebHostDefaults overrides a host specific setting (the application name).

        bootstrapHostBuilder.ConfigureHostConfiguration(config =>
        {
            if (args is { Length: > 0 })
            {
                config.AddCommandLine(args);
            }

            // Apply the options after the args
            options.ApplyHostConfiguration(config);
        });


        Configuration = new ConfigurationManager();

        // Collect the hosted services separately since we want those to run after the user's hosted services
        _services.TrackHostedServices = true;

        // This is the application configuration
        (HostBuilderContext hostContext, ConfigurationManager hostConfiguration) =
            bootstrapHostBuilder.RunDefaultCallbacks(Configuration, _hostBuilder);

        // Stop tracking here
        _services.TrackHostedServices = false;

        // Capture the host configuration values here. We capture the values so that
        // changes to the host configuration have no effect on the final application. The
        // host configuration is immutable at this point.
        _hostConfigurationValues = new List<KeyValuePair<string, string>>(hostConfiguration.AsEnumerable()!);

        // Grab the IHostEnvironment from the hostContext. This also matches the instance in the IServiceCollection.
        Environment = hostContext.HostingEnvironment;
        Logging = new LoggingBuilder(Services);
        Host = new ConfigureHostBuilder(hostContext, Configuration, Services);

        Services.AddSingleton<IConfiguration>(Configuration);
    }

    /// <summary>
    ///     An <see cref="IHostBuilder" /> for configuring host specific properties, but not building.
    ///     To build after configuration, call <see cref="Build" />.
    /// </summary>
    public ConfigureHostBuilder Host { get; }

    /// <summary>
    ///     Provides information about the web hosting environment an application is running.
    /// </summary>
    public IHostEnvironment Environment { get; }

    /// <summary>
    ///     A collection of services for the application to compose. This is useful for adding user provided or framework
    ///     provided services.
    /// </summary>
    public IServiceCollection Services { get; }

    IConfiguration Abstractions.ITelegramApplicationBuilder.Configuration => Configuration;

    /// <summary>
    ///     A collection of configuration providers for the application to compose. This is useful for adding new configuration
    ///     sources and providers.
    /// </summary>
    public ConfigurationManager Configuration { get; }

    /// <summary>
    ///     A collection of logging providers for the application to compose. This is useful for adding new logging providers.
    /// </summary>
    public ILoggingBuilder Logging { get; }

    public IRequiredUpdates RequiredUpdates { get; } = new RequiredUpdates();

    /// <summary>
    ///     Builds the <see cref="TelegramApplicationBuilder{TContext}" />.
    /// </summary>
    /// <returns>A configured <see cref="TelegramApplicationBuilder{TContext}" />.</returns>
    public TelegramApplication<TContext> Build()
    {
        // Wire up the host configuration here. We don't try to preserve the configuration
        // source itself here since we don't support mutating the host values after creating the builder.
        _hostBuilder.ConfigureHostConfiguration(builder =>
        {
            builder.AddInMemoryCollection(_hostConfigurationValues!);
        });

        // Wire up the application configuration by copying the already built configuration providers over to final configuration builder.
        // We wrap the existing provider in a configuration source to avoid re-bulding the already added configuration sources.
        _hostBuilder.ConfigureAppConfiguration(builder =>
        {
            foreach (IConfigurationProvider? provider in ((IConfigurationRoot)Configuration).Providers)
            {
                builder.Sources.Add(new ConfigurationProviderSource(provider));
            }

            foreach ((string? key, object? value) in ((IConfigurationBuilder)Configuration).Properties)
            {
                builder.Properties[key] = value;
            }
        });

        // This needs to go here to avoid adding the IHostedService that boots the server twice (the GenericWebHostService).
        // Copy the services that were added via TelegramApplicationBuilder.Services into the final IServiceCollection
        _hostBuilder.ConfigureServices((_, services) =>
        {
            _pipelineBuilder = services.AddTelegramServices<TContext>(RequiredUpdates);

            // We've only added services configured by the GenericWebHostBuilder and WebHost.ConfigureWebDefaults
            // at this point. HostBuilder news up a new ServiceCollection in HostBuilder.Build() we haven't seen
            // until now, so we cannot clear these services even though some are redundant because
            // we called ConfigureWebHostDefaults on both the _deferredHostBuilder and _hostBuilder.
            foreach (ServiceDescriptor s in _services)
            {
                // Skip the configuration manager instance we added earlier
                // we're already going to wire it up to this new configuration source
                // after we've built the application. There's a chance the user manually added
                // this as well but we still need to remove it from the final configuration
                // to avoid cycles in the configuration graph
                if ((s.ServiceType == typeof(IConfiguration)) &&
                    (s.ImplementationInstance == Configuration))
                {
                    continue;
                }

                services.Add(s);
            }

            // Add the hosted services that were initially added last
            // this makes sure any hosted services that are added run after the initial set
            // of hosted services. This means hosted services run before the web host starts.
            foreach (ServiceDescriptor s in _services.HostedServices)
            {
                services.Add(s);
            }

            // Clear the hosted services list out
            _services.HostedServices.Clear();

            // Add any services to the user visible service collection so that they are observable
            // just in case users capture the Services property. Orchard does this to get a "blueprint"
            // of the service collection

            // Drop the reference to the existing collection and set the inner collection
            // to the new one. This allows code that has references to the service collection to still function.
            _services.InnerCollection = services;
        });

        // Run the other callbacks on the final host builder
        Host.RunDeferredCallbacks(_hostBuilder);

        _builtApplication = new TelegramApplication<TContext>(_hostBuilder.Build(), ref _pipelineBuilder!);

        // Make builder.Configuration match the final configuration. To do that
        // we clear the sources and add the built configuration as a source
        Configuration.Sources.Clear();
        Configuration.AddConfiguration(_builtApplication.Configuration);

        // Mark the service collection as read-only to prevent future modifications
        _services.IsReadOnly = true;

        return _builtApplication;
    }

    // private void ConfigureApplication(HostBuilderContext context, IApplicationBuilder<TContext> app)
    // {
    //     Debug.Assert(_builtApplication is not null);
    // }

    private sealed class LoggingBuilder : ILoggingBuilder
    {
        public LoggingBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }
    }

    private sealed class ConfigurationProviderSource : IConfigurationSource
    {
        private readonly IConfigurationProvider _configurationProvider;

        public ConfigurationProviderSource(IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return _configurationProvider;
        }
    }
}