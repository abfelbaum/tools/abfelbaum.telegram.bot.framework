using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Middlewares;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Middlewares;

public class UpdateHandlingMiddleware<TContext> : ITelegramMiddleware<TContext>
    where TContext : DefaultContext
{
    private readonly ILogger<UpdateHandlingMiddleware<TContext>> _logger;
    private readonly IServiceProvider _provider;
    private readonly IEnumerable<TelegramUpdate> _updateHandlers;

    public UpdateHandlingMiddleware(IEnumerable<TelegramUpdate> updateHandlers,
        ILogger<UpdateHandlingMiddleware<TContext>> logger, IServiceProvider provider)
    {
        _updateHandlers = updateHandlers;
        _logger = logger;
        _provider = provider;
    }

    public async Task InvokeAsync(TContext context, NextMiddleware next,
        CancellationToken cancellationToken = new())
    {
        IEnumerable<TelegramUpdate> updateHandlers = _updateHandlers.Where(s => s.RequiredUpdates.Any(u => u == context.Update.Type));

        foreach (TelegramUpdate settings in updateHandlers)
        {
            try
            {
                Type? updateHandler = await settings.GetUpdateHandler(context.Update).ConfigureAwait(false);

                if (updateHandler is null)
                {
                    continue;
                }

                Type? preprocessorType = GetPreprocessor(settings);
                object? parameter = GetParameter(preprocessorType, context.Update);
                object? handler = ActivatorUtilities.GetServiceOrCreateInstance(_provider, updateHandler);

                // ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
                if (handler is null)
                {
                    continue;
                }

                ConfiguredTaskAwaitable? task = ExecuteMethod<Task>(handler, "InvokeAsync", [parameter, cancellationToken])?
                    .ConfigureAwait(false);

                if (!task.HasValue)
                {
                    continue;
                }

                await task.Value;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e, "An error occurred while handling update with {Command}",
                    settings.GetType().FullName);
            }
        }

        await context.RequestEnded().ConfigureAwait(false);
    }

    private static TResult? ExecuteMethod<TResult>(object instance, string methodName, object?[] args)
    {
        object? objectResult = ExecuteMethod(instance, methodName, args);

        return objectResult is TResult result ? result : default;
    }

    private static object? ExecuteMethod(object instance, string methodName, object?[] args)
    {
        MethodInfo? method = instance.GetType().GetMethod(methodName);

        object? objectResult = method?.Invoke(instance, args);

        return objectResult;
    }

    private static Type? GetPreprocessor(object options)
    {
        return ExecuteMethod<Type>(options, "GetPreprocessor", []);
    }

    private object? GetParameter(Type? preprocessorType, Update update)
    {
        if (preprocessorType is null)
        {
            return update;
        }

        object preprocessor = ActivatorUtilities.GetServiceOrCreateInstance(_provider, preprocessorType);

        return ExecuteMethod(preprocessor, "Preprocess", [update]);
    }
}