using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.Middlewares;

public class ContextMiddleware<TContext> : IMiddleware<TContext> where TContext : DefaultContext
{
    private readonly IBotService _botService;

    public ContextMiddleware(IBotService botService)
    {
        _botService = botService;
    }

    public async Task InvokeAsync(TContext context, NextMiddleware next,
        CancellationToken cancellationToken = new())
    {
        context.Bot = await _botService.Get().ConfigureAwait(false);
        context.Chat = context.Update.GetChat();

        await next().ConfigureAwait(false);
    }
}