using System;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Types;

public record BotUser
{
    /// <summary>
    ///     Unique identifier for this user or bot
    /// </summary>
    public long Id { get; init; }

    /// <summary>
    ///     User's or bot's first name
    /// </summary>
    public string FirstName { get; init; } = null!;

    /// <summary>
    ///     Optional. User's or bot's last name
    /// </summary>
    public string? LastName { get; init; }

    /// <summary>
    ///     Optional. User's or bot's username
    /// </summary>
    public string Username { get; init; } = null!;

    /// <summary>
    ///     Optional. True, if the bot can be invited to groups. Returned only in
    ///     <see cref="GetMeRequest" />
    /// </summary>
    public bool CanJoinGroups { get; init; }

    /// <summary>
    ///     Optional. True, if privacy mode is disabled for the bot. Returned only in
    ///     <see cref="GetMeRequest" />
    /// </summary>
    public bool CanReadAllGroupMessages { get; init; }

    /// <summary>
    ///     Optional. True, if the bot supports inline queries. Returned only in
    ///     <see cref="GetMeRequest" />
    /// </summary>
    public bool SupportsInlineQueries { get; init; }

    /// <inheritdoc />
    public override string ToString()
    {
        return $"@{Username} ({Id})";
    }

    public static explicit operator BotUser(User user)
    {
        return new BotUser
        {
            Id = user.Id,
            Username = user.Username ?? throw new ArgumentNullException(nameof(user.Username)),
            FirstName = user.FirstName,
            LastName = user.LastName,
            CanJoinGroups = user.CanJoinGroups,
            SupportsInlineQueries = user.SupportsInlineQueries,
            CanReadAllGroupMessages = user.CanReadAllGroupMessages
        };
    }
}