using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Types;

public class DefaultContext
{
    public IDictionary<object, object?> Items = new Dictionary<object, object?>();
    public Chat? Chat { get; set; }
    public BotUser Bot { get; set; } = null!;
    public Update Update { get; set; } = null!;

    public IList<RequestEndedDelegate> RequestEndedActions { get; } = new List<RequestEndedDelegate>();

    public async ValueTask RequestEnded()
    {
        foreach (RequestEndedDelegate @delegate in RequestEndedActions)
        {
            await @delegate().ConfigureAwait(false);
        }
    }
}