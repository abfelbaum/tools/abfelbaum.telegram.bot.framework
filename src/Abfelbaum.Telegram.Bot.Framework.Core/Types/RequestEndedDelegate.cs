using System.Threading.Tasks;

namespace Abfelbaum.Telegram.Bot.Framework.Types;

public delegate ValueTask RequestEndedDelegate();