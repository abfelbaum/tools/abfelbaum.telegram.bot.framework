using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class ChosenInlineResultPreprocessor : IUpdatePreprocessor<ChosenInlineResult>
{
    public ChosenInlineResult Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.ChosenInlineResult);

        return update.ChosenInlineResult;
    }
}