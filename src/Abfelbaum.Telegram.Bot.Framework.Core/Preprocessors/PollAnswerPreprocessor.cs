using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class PollAnswerPreprocessor : IUpdatePreprocessor<PollAnswer>
{
    public PollAnswer Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.PollAnswer);

        return update.PollAnswer;
    }
}