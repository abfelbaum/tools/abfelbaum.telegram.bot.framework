using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class ChatMemberPreprocessor : IUpdatePreprocessor<ChatMemberUpdated>
{
    public ChatMemberUpdated Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.ChatMember);

        return update.ChatMember;
    }
}