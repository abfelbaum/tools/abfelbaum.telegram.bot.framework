using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class ChatJoinRequestPreprocessor : IUpdatePreprocessor<ChatJoinRequest>
{
    public ChatJoinRequest Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.ChatJoinRequest);

        return update.ChatJoinRequest;
    }
}