using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Payments;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class PreCheckoutQueryPreprocessor : IUpdatePreprocessor<PreCheckoutQuery>
{
    public PreCheckoutQuery Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.PreCheckoutQuery);

        return update.PreCheckoutQuery;
    }
}