using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class PollPreprocessor : IUpdatePreprocessor<Poll>
{
    public Poll Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.Poll);

        return update.Poll;
    }
}