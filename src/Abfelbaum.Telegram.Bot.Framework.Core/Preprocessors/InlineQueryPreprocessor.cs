using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class InlineQueryPreprocessor : IUpdatePreprocessor<InlineQuery>
{
    public InlineQuery Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.InlineQuery);

        return update.InlineQuery;
    }
}