using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Payments;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class ShippingQueryPreprocessor : IUpdatePreprocessor<ShippingQuery>
{
    public ShippingQuery Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.ShippingQuery);

        return update.ShippingQuery;
    }
}