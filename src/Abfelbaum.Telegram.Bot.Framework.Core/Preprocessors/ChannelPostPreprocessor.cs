using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Preprocessors;

public class ChannelPostPreprocessor : IUpdatePreprocessor<Message>
{
    public Message Preprocess(Update update)
    {
        ArgumentNullException.ThrowIfNull(update.ChannelPost);

        return update.ChannelPost;
    }
}