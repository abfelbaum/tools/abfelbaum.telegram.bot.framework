using Abfelbaum.Telegram.Bot.Framework.Types;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

/// <summary>
///     Defines a class that provides the mechanisms to configure an application's request pipeline.
/// </summary>
public interface IApplicationBuilder<TContext> : IPipelineBuilder<TContext> where TContext : DefaultContext;