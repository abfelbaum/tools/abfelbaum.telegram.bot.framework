using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface ITelegramUpdate : ITelegramUpdate<Update>
{
}

public interface ITelegramUpdate<in TArgs>
{
    public Task InvokeAsync(TArgs args, CancellationToken cancellationToken = new());
}