using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface IBotService
{
    public Task<BotUser> Get(CancellationToken cancellationToken = new());
}