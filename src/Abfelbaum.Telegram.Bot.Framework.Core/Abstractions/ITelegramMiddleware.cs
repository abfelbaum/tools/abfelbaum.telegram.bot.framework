using Abfelbaum.Telegram.Bot.Framework.Types;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface ITelegramMiddleware<TContext> : IMiddleware<TContext>
    where TContext : DefaultContext
{
}