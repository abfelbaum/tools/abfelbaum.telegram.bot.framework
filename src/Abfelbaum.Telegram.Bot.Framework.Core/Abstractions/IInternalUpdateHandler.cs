using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface IInternalUpdateHandler
{
    public UpdateType[]? AllowedUpdates { get; }
    Task HandleUpdateAsync(Update update, CancellationToken cancellationToken = new());
    Task HandleErrorAsync(Exception exception, CancellationToken cancellationToken = new());
}