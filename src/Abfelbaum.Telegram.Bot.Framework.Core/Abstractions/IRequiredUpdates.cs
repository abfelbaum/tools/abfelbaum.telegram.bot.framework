using System.Collections.Generic;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface IRequiredUpdates : ISet<UpdateType>
{
}