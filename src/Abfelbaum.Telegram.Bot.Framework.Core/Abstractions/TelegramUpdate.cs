using System;
using System.Reflection;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public abstract class TelegramUpdate<TPreprocessor, TArgs> : TelegramUpdate<TArgs>
    where TPreprocessor : IUpdatePreprocessor<TArgs>
{
    public Type GetPreprocessor()
    {
        return typeof(TPreprocessor);
    }
}

public abstract class TelegramUpdate<TArgs> : TelegramUpdate
{
    internal override void ValidateUpdateHandlerType(Type type)
    {
        if (!typeof(ITelegramUpdate<TArgs>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
        {
            throw new ArgumentException($"Handler does not implement type {typeof(ITelegramUpdate<TArgs>).FullName}");
        }
    }
}

public abstract class TelegramUpdate
{
    public abstract UpdateType[] RequiredUpdates { get; }

    public async Task<Type?> GetUpdateHandler(Update update)
    {
        Type? type = await GetUpdateHandlerType(update).ConfigureAwait(false);

        if (type is null)
        {
            return null;
        }

        ValidateUpdateHandlerType(type);

        return type;
    }

    internal abstract void ValidateUpdateHandlerType(Type type);

    protected abstract Task<Type?> GetUpdateHandlerType(Update update);
}