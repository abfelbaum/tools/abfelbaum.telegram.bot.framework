using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface IUpdatePreprocessor<out TResult>
{
    public TResult Preprocess(Update update);
}