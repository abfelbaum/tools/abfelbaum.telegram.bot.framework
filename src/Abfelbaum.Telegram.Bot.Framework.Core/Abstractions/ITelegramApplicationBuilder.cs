using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Abfelbaum.Telegram.Bot.Framework.Abstractions;

public interface ITelegramApplicationBuilder
{
    public IRequiredUpdates RequiredUpdates { get; }

    /// <summary>
    ///     Provides information about the web hosting environment an application is running.
    /// </summary>
    public IHostEnvironment Environment { get; }

    /// <summary>
    ///     A collection of services for the application to compose. This is useful for adding user provided or framework
    ///     provided services.
    /// </summary>
    public IServiceCollection Services { get; }

    /// <summary>
    ///     A collection of configuration providers for the application to compose. This is useful for adding new configuration
    ///     sources and providers.
    /// </summary>
    public IConfiguration Configuration { get; }
}