using System;
using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Updates;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Extensions;

public static class TelegramApplicationBuilderExtensions
{
    public static ITelegramApplicationBuilder AddUpdate<TUpdateSettings>(this ITelegramApplicationBuilder builder,
        UpdateType requiredUpdate, params UpdateType[] requiredUpdates)
        where TUpdateSettings : TelegramUpdate
    {
        builder.RequiredUpdates.Add(requiredUpdate);

        foreach (UpdateType update in requiredUpdates)
        {
            builder.RequiredUpdates.Add(update);
        }

        builder.Services.AddTransient<TelegramUpdate, TUpdateSettings>();

        return builder;
    }

    public static ITelegramApplicationBuilder AddUpdate<TUpdateSettings>(this ITelegramApplicationBuilder builder,
        Func<IServiceProvider, TUpdateSettings> implementationFactory, UpdateType requiredUpdate,
        params UpdateType[] requiredUpdates)
        where TUpdateSettings : TelegramUpdate
    {
        builder.RequiredUpdates.Add(requiredUpdate);

        foreach (UpdateType update in requiredUpdates)
        {
            builder.RequiredUpdates.Add(update);
        }

        builder.Services.AddTransient<TelegramUpdate>(implementationFactory);

        return builder;
    }

    public static ITelegramApplicationBuilder AddSimpleUpdate<THandler, TArgs>(this ITelegramApplicationBuilder builder,
        UpdateType requiredUpdate, params UpdateType[] requiredUpdates)
        where THandler : ITelegramUpdate<TArgs>
    {
        var requiredUpdatesList = new List<UpdateType>(requiredUpdates)
        {
            requiredUpdate
        };

        foreach (UpdateType update in requiredUpdatesList)
        {
            builder.RequiredUpdates.Add(update);
        }

        builder.Services.AddTransient<TelegramUpdate>(_ =>
            new SimpleUpdate<THandler, TArgs>(requiredUpdatesList.ToArray()));

        return builder;
    }

    public static ITelegramApplicationBuilder AddSimpleUpdate<THandler, TPreprocessor, TArgs>(
        this ITelegramApplicationBuilder builder,
        UpdateType requiredUpdate, params UpdateType[] requiredUpdates)
        where THandler : ITelegramUpdate<TArgs>
        where TPreprocessor : IUpdatePreprocessor<TArgs>
    {
        var requiredUpdatesList = new List<UpdateType>(requiredUpdates)
        {
            requiredUpdate
        };

        foreach (UpdateType update in requiredUpdatesList)
        {
            builder.RequiredUpdates.Add(update);
        }

        builder.Services.AddTransient<TelegramUpdate>(_ =>
            new SimpleUpdate<THandler, TPreprocessor, TArgs>(requiredUpdatesList.ToArray()));

        return builder;
    }
}