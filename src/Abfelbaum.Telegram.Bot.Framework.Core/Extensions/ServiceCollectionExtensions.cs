using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Configuration;
using Abfelbaum.Telegram.Bot.Framework.Middlewares;
using Abfelbaum.Telegram.Bot.Framework.Services;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Middlewares;
using Telegram.Bot;

namespace Abfelbaum.Telegram.Bot.Framework.Extensions;

public static class ServiceCollectionExtensions
{
    public static void AddBot(this IServiceCollection services, IConfiguration configuration)
    {
        OptionsBuilder<BotOptions> optionsBuilder = services.AddBot();

        optionsBuilder.Bind(configuration);
    }

    public static void AddBot(this IServiceCollection services, Action<BotOptions> configureOptions)
    {
        OptionsBuilder<BotOptions> optionsBuilder = services.AddBot();

        optionsBuilder.Configure(configureOptions);
    }

    public static OptionsBuilder<BotOptions> AddBot(this IServiceCollection services)
    {
        services.AddLogging();
        services.AddOptions();

        services.AddPipeline();
        services.AddTelegramClient();

        return services.AddOptions<BotOptions>();
    }

    // ReSharper disable once UnusedMethodReturnValue.Local
    private static IServiceCollection AddPipeline(this IServiceCollection services)
    {
        services.TryAddSingleton<IBotService, BotService>();

        return services;
    }

    // ReSharper disable once UnusedMethodReturnValue.Local
    private static IServiceCollection AddTelegramClient(this IServiceCollection services)
    {
        services.AddHttpClient();

        services.AddHttpClient(nameof(ITelegramBotClient))
            .AddTypedClient<ITelegramBotClient>((httpClient, provider) =>
            {
                BotOptions options = provider.GetRequiredService<IOptions<BotOptions>>().Value;
                return new TelegramBotClient(new TelegramBotClientOptions(options.Token, options.BaseUrl, options.UseTestEnvironment), httpClient);
            });

        return services;
    }

    public static IPipelineBuilder<TContext> AddTelegramServices<TContext>(this IServiceCollection services,
        IRequiredUpdates? requiredUpdates = null)
        where TContext : DefaultContext
    {
        requiredUpdates ??= new RequiredUpdates();

        services.TryAddTransient<IInternalUpdateHandler, InternalUpdateHandler<TContext>>();

        services.TryAddScoped<TContext, TContext>();
        services.TryAddScoped<DefaultContext>(provider => provider.GetRequiredService<TContext>());

        services.TryAddSingleton(_ => requiredUpdates);

        IPipelineBuilder<TContext> requestPipeline = services.ConfigurePipelineFor<TContext>();

        requestPipeline.Use<ContextMiddleware<TContext>>();

        return requestPipeline;
    }
}