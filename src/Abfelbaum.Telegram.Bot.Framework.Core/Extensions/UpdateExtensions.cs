using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Extensions;

public static class UpdateExtensions
{
    public static long? GetChatId(this Update update)
    {
        return update.GetChat()?.Id ?? 0;
    }

    public static Chat? GetChat(this Update update)
    {
        return update.Type switch
        {
            UpdateType.Message => update.Message?.Chat,
            UpdateType.CallbackQuery when update.CallbackQuery?.Message is Message message => message.Chat,
            UpdateType.EditedMessage => update.EditedMessage?.Chat,
            UpdateType.ChannelPost => update.ChannelPost?.Chat,
            UpdateType.EditedChannelPost => update.EditedChannelPost?.Chat,
            UpdateType.MyChatMember => update.MyChatMember?.Chat,
            UpdateType.ChatMember => update.ChatMember?.Chat,
            UpdateType.ChatJoinRequest => update.ChatJoinRequest?.Chat,

            UpdateType.Unknown => null,
            UpdateType.InlineQuery => null,
            UpdateType.ChosenInlineResult => null,
            UpdateType.ShippingQuery => null,
            UpdateType.PreCheckoutQuery => null,
            UpdateType.Poll => null,
            UpdateType.PollAnswer => null,
            _ => null
        };
    }
}