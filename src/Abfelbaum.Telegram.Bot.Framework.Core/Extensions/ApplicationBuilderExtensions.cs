using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Middlewares;
using Abfelbaum.Telegram.Bot.Framework.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Extensions;

public static class ApplicationBuilderExtensions
{
    /// <summary>
    ///     Enables the main update handler. Should be executed at the end of your pipeline.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="TContext"></typeparam>
    /// <returns></returns>
    public static IApplicationBuilder<TContext> UseUpdateHandling<TContext>(this IApplicationBuilder<TContext> builder)
        where TContext : DefaultContext
    {
        builder.Use<UpdateHandlingMiddleware<TContext>>();

        return builder;
    }
}