namespace Abfelbaum.Telegram.Bot.Framework.Configuration;

public record BotOptions
{
    public required string Token { get; set; }
    public required string BaseUrl { get; set; }
    public required bool UseTestEnvironment { get; set; }
}