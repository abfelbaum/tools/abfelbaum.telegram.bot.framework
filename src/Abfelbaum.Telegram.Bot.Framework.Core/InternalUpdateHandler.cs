using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Middlewares;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework;

public class InternalUpdateHandler<TContext> : IInternalUpdateHandler
    where TContext : DefaultContext
{
    private readonly ILogger<InternalUpdateHandler<TContext>> _logger;
    private readonly IRequiredUpdates _requiredUpdates;
    private readonly IServiceScopeFactory _scopeFactory;

    public InternalUpdateHandler(ILogger<InternalUpdateHandler<TContext>> logger, IServiceScopeFactory scopeFactory,
        IRequiredUpdates requiredUpdates)
    {
        _logger = logger;
        _scopeFactory = scopeFactory;
        _requiredUpdates = requiredUpdates;
    }

    public async Task HandleUpdateAsync(Update update, CancellationToken cancellationToken = new())
    {
        AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();
        IPipeline<TContext> pipeline = scope.ServiceProvider.GetRequiredService<IPipeline<TContext>>();
        TContext context = scope.ServiceProvider.GetRequiredService<TContext>();

        context.RequestEndedActions.Add(() => scope.DisposeAsync());
        context.Update = update;

        try
        {
            await pipeline.ExecuteAsync(context, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "An error occurred while running pipeline: {Message}", e.Message);
            await context.RequestEnded().ConfigureAwait(false);
        }
    }

    public Task HandleErrorAsync(Exception exception, CancellationToken cancellationToken = new())
    {
        string errorMessage = exception switch
        {
            ApiRequestException e => $"Telegram API Error:\n[{e.ErrorCode}]\n{e.Message}",
            _ => exception.ToString()
        };

        // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
        _logger.LogError(errorMessage);
        return Task.CompletedTask;
    }

    public UpdateType[] AllowedUpdates => _requiredUpdates.ToArray();
}