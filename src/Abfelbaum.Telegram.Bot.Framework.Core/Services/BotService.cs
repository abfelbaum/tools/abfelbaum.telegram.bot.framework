using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Telegram.Bot;
using Telegram.Bot.Requests;

namespace Abfelbaum.Telegram.Bot.Framework.Services;

public class BotService : IBotService
{
    private readonly ITelegramBotClient _client;
    private BotUser? _bot;

    public BotService(ITelegramBotClient client)
    {
        _client = client;
    }

    public async Task<BotUser> Get(CancellationToken cancellationToken = new())
    {
        if (_bot is not null)
        {
            return _bot;
        }

        _bot = (BotUser) await _client.MakeRequestAsync(new GetMeRequest(), cancellationToken: cancellationToken).ConfigureAwait(false);

        return _bot;
    }
}