using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Services;

public class RequiredUpdates : HashSet<UpdateType>, IRequiredUpdates
{
}