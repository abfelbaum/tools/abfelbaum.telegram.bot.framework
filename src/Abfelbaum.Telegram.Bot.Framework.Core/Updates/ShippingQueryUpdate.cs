using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Preprocessors;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.Payments;

namespace Abfelbaum.Telegram.Bot.Framework.Updates;

public abstract class ShippingQueryUpdate : TelegramUpdate<ShippingQueryPreprocessor, ShippingQuery>
{
    public override UpdateType[] RequiredUpdates => [UpdateType.ShippingQuery];
}