using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Preprocessors;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.Payments;

namespace Abfelbaum.Telegram.Bot.Framework.Updates;

public abstract class
    PreCheckoutQueryUpdate : TelegramUpdate<PreCheckoutQueryPreprocessor, PreCheckoutQuery>
{
    public override UpdateType[] RequiredUpdates => [UpdateType.PreCheckoutQuery];
}