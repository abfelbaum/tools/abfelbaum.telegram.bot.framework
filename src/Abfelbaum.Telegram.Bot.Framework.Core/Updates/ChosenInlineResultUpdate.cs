using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Preprocessors;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Updates;

public abstract class
    ChosenInlineResultUpdate : TelegramUpdate<ChosenInlineResultPreprocessor, ChosenInlineResult>
{
    public override UpdateType[] RequiredUpdates => [UpdateType.ChosenInlineResult];
}