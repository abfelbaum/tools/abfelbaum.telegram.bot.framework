using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Updates;

public class SimpleUpdate<THandler, TArgs> : TelegramUpdate<TArgs>
    where THandler : ITelegramUpdate<TArgs>
{
    public SimpleUpdate(UpdateType[] requiredUpdates)
    {
        RequiredUpdates = requiredUpdates;
    }

    public override UpdateType[] RequiredUpdates { get; }

    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        return Task.FromResult<Type?>(typeof(THandler));
    }
}

public sealed class SimpleUpdate<THandler, TPreprocessor, TArgs> : TelegramUpdate<TPreprocessor, TArgs>
    where THandler : ITelegramUpdate<TArgs>
    where TPreprocessor : IUpdatePreprocessor<TArgs>
{
    public SimpleUpdate(UpdateType[] requiredUpdates)
    {
        RequiredUpdates = requiredUpdates;
    }

    public override UpdateType[] RequiredUpdates { get; }

    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        return Task.FromResult<Type?>(typeof(THandler));
    }
}