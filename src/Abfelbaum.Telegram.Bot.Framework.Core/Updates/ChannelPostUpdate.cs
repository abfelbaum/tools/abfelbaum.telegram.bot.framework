using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Preprocessors;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Updates;

public abstract class ChannelPostUpdate : TelegramUpdate<ChannelPostPreprocessor, Message>
{
    public override UpdateType[] RequiredUpdates => [UpdateType.ChannelPost];
}