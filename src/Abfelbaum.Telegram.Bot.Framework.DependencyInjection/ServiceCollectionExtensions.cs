using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Services;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.DependencyInjection;

public static class ServiceCollectionExtensions
{
    public static TelegramApplicationBuilder<TContext> AddTelegramDependencyInjectionServices<TContext>(
        this IServiceCollection services, IHostEnvironment environment, IConfiguration configuration,
        IRequiredUpdates? requiredUpdates = null)
        where TContext : DefaultContext
    {
        requiredUpdates ??= new RequiredUpdates();

        IPipelineBuilder<TContext> pipelineBuilder = services.AddTelegramServices<TContext>(requiredUpdates);

        return new TelegramApplicationBuilder<TContext>(pipelineBuilder, requiredUpdates, environment, services,
            configuration);
    }
}