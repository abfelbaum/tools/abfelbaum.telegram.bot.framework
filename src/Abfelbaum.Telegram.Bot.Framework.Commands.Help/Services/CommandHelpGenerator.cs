using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Commands;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Localization;
using Telegram.Bot.Types;
using ICommandHelpGenerator = Abfelbaum.Telegram.Bot.Framework.Commands.Help.Abstractions.ICommandHelpGenerator;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Services;

public class CommandHelpGenerator : ICommandHelpGenerator
{
    private readonly IEnumerable<ITelegramCommand> _commands;
    private readonly IScopeMatchingService _scopeMatchingService;
    private readonly IStringLocalizer<CommandHelpGenerator> _localizer;

    public CommandHelpGenerator(IEnumerable<ITelegramCommand> commands, IScopeMatchingService scopeMatchingService, IStringLocalizer<CommandHelpGenerator> localizer)
    {
        _commands = commands;
        _scopeMatchingService = scopeMatchingService;
        _localizer = localizer;
    }

    public async Task<StringBuilder> Generate(Chat chat, User? user, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new($"<b>{_localizer["AvailableCommands"]}</b>\n");

        await foreach (ITelegramCommand command in GetCommands(chat, user, cancellationToken).ConfigureAwait(false))
        {
            builder.Append($"\n<code>{WebUtility.HtmlEncode(command.Documentation.GetName())}</code>");

            string shortDescription = command.Documentation.GetShortDescription();
            if (!string.IsNullOrWhiteSpace(shortDescription))
            {
                builder.Append($" - {WebUtility.HtmlEncode(shortDescription)}");
            }
        }

        builder.AppendFormat($"\n\n{_localizer["MoreInformation"]}", GetHelpCommandHintText());

        return builder;
    }

    private StringBuilder GetHelpCommandHintText()
    {
        HelpCommand? helpCommand = _commands.Where(x => x is HelpCommand).Cast<HelpCommand>().First();

        StringBuilder builder = new("<code>");
        builder.Append(WebUtility.HtmlEncode($"{helpCommand.Documentation.GetName()} <{_localizer["CommandHint"]}>"));
        builder.Append("</code>");

        return builder;
    }

    public async Task<StringBuilder?> Generate(string? search, Chat chat, User? user,
        CancellationToken cancellationToken = new())
    {
        if (search is null)
        {
            return null;
        }

        ITelegramCommand? command = await GetCommands(chat, user, cancellationToken)
            .FirstOrDefaultAsync(command => command.IsCommandName(search), cancellationToken).ConfigureAwait(false);

        if (command is null)
        {
            return null;
        }

        StringBuilder builder = new();
        builder.Append($"<code>/{WebUtility.HtmlEncode(command.Documentation.GetName())}</code>:\n\n");
        builder.Append($"{WebUtility.HtmlDecode(command.Documentation.GetLongDescription())}\n\n");

        List<string> aliases = GetAliases(command.Documentation).ToList();

        if (aliases.Count != 0)
        {
            builder.Append($"{_localizer["Aliases"]}:");

            foreach (string alias in aliases)
            {
                builder.Append($"\n<code>/{WebUtility.HtmlEncode(alias)}</code>");
            }

            builder.Append("\n\n");
        }


        builder.Append($"{_localizer["Usages"]}:");

        foreach (string usage in command.Documentation.GetUsages())
        {
            builder.Append($"\n<code>/{WebUtility.HtmlEncode(usage)}</code>");
        }

        return builder;
    }

    private IEnumerable<string> GetAliases(CommandDocumentation documentation)
    {
        List<string> aliases = [..documentation.GetAliases()];

        foreach (CultureInfo cultureInfo in documentation.CultureInfos)
        {
            aliases.Add(documentation.GetName(cultureInfo));
            aliases.AddRange(documentation.GetAliases(cultureInfo));
        }

        string commandName = documentation.GetName();
        return aliases.Distinct().Where(x => x != commandName);
    }

    private IAsyncEnumerable<ITelegramCommand> GetCommands(Chat chat, User? user,
        CancellationToken cancellationToken = new())
    {
        return from command in GetScopedCommands(chat, user, cancellationToken)
            let name = command.Documentation.GetName()
            where !string.IsNullOrWhiteSpace(name)
            orderby name
            select command;
    }

    private async IAsyncEnumerable<ITelegramCommand> GetScopedCommands(Chat chat, User? user,
        [EnumeratorCancellation] CancellationToken cancellationToken = new())
    {
        foreach (ITelegramCommand command in _commands)
        {
            if (!await _scopeMatchingService
                    .ScopesContain(command.Handlers.SelectMany(x => x.Scopes), chat, user, cancellationToken)
                    .ConfigureAwait(false))
            {
                continue;
            }

            yield return command;
        }
    }
}