using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Abstractions;
using Microsoft.Extensions.Localization;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Handlers;

public class SpecificHelpCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;
    private readonly IStringLocalizer<SpecificHelpCommandHandler> _localizer;
    private readonly ICommandHelpGenerator _helpGenerator;

    public SpecificHelpCommandHandler(ICommandHelpGenerator helpGenerator, ITelegramBotClient client, IStringLocalizer<SpecificHelpCommandHandler> localizer)
    {
        _helpGenerator = helpGenerator;
        _client = client;
        _localizer = localizer;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        message.TryGetCommandArguments(out string? arguments);

        StringBuilder builder = new();
        StringBuilder? helpText = await _helpGenerator
            .Generate(arguments, message.Chat, message.From, cancellationToken).ConfigureAwait(false);

        if (helpText is not null)
        {
            builder.Append(helpText);
        }
        else
        {
            builder.AppendFormat($"{_localizer["CommandNotFound"]}\n\n", arguments);
            builder.Append(await _helpGenerator.Generate(message.Chat, message.From, cancellationToken).ConfigureAwait(false));
        }

        var request = new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = builder.ToString(),
            ParseMode = ParseMode.Html,
            ReplyParameters = new ReplyParameters
            {
                MessageId = message.MessageId
            }
        };

        await _client.MakeRequestAsync(request, cancellationToken).ConfigureAwait(false);
    }
}