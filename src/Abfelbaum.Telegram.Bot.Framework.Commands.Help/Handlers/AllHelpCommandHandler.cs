using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using SendMessageRequest = Telegram.Bot.Requests.SendMessageRequest;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Handlers;

public class AllHelpCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;
    private readonly ICommandHelpGenerator _helpGenerator;

    public AllHelpCommandHandler(ICommandHelpGenerator helpGenerator, ITelegramBotClient client)
    {
        _helpGenerator = helpGenerator;
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = await _helpGenerator.Generate(message.Chat, message.From, cancellationToken)
            .ConfigureAwait(false);

        var request = new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = builder.ToString(),
            ParseMode = ParseMode.Html,
            ReplyParameters = new ReplyParameters
            {
                MessageId = message.MessageId
            }
        };

        await _client.MakeRequestAsync(request, cancellationToken: cancellationToken).ConfigureAwait(false);
    }
}