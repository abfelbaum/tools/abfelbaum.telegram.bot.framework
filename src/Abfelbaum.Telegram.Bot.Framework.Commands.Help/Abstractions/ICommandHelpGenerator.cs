using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Abstractions;

public interface ICommandHelpGenerator
{
    public Task<StringBuilder> Generate(Chat chat, User? user, CancellationToken cancellationToken = new());

    public Task<StringBuilder?> Generate(string? search, Chat chat, User? user,
        CancellationToken cancellationToken = new());
}