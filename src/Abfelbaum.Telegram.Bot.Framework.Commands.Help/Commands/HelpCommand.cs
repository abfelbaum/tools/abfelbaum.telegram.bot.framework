using System.Collections.Generic;
using System.Globalization;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Handlers;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Localization;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Commands;

public sealed class HelpCommand : TelegramCommand
{
    public HelpCommand(IBotService botService, IScopeMatchingService scopeMatchingService, IStringLocalizer<HelpCommand> localizer) : base(botService,
        scopeMatchingService)
    {
        Documentation = new CommandDocumentation
        {
            Name = "Name",
            ShortDescription = "ShortDescription",
            LongDescription = "LongDescription",
            Usages =
            {
                "Name",
                "NameWithArgumentPlaceholder"
            },
            Localizer = localizer,
            CultureInfos =
            {
                CultureInfo.GetCultureInfo("en"),
                CultureInfo.GetCultureInfo("de")
            }
        };
    }

    public override CommandDocumentation Documentation { get; init; }

    public override IEnumerable<CommandHandler> Handlers => new List<CommandHandler>
    {
        new CommandHandler<AllHelpCommandHandler>(CommandScope.Default()),
        new CommandHandler<SpecificHelpCommandHandler>(CommandScope.Default())
        {
            Selector = update => update.Message?.TryGetCommandArguments(out string? _) ?? false
        }
    };
}