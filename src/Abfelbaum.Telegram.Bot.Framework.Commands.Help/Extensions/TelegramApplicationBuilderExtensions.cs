﻿using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Commands;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Abfelbaum.Telegram.Bot.Framework.Commands.Help.Extensions;

public static class TelegramApplicationBuilderExtensions
{
    public static ITelegramApplicationBuilder AddHelpCommand(this ITelegramApplicationBuilder builder)
    {
        builder.Services.TryAddTransient<ICommandHelpGenerator, CommandHelpGenerator>();
        builder.AddCommand<HelpCommand>();

        builder.Services.AddLocalization(options =>
        {
            options.ResourcesPath = "Resources";
        });

        return builder;
    }
}