using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.AspNetCore;

public class TelegramApplicationBuilder<TContext> : ITelegramApplicationBuilder, IApplicationBuilder<TContext>
    where TContext : DefaultContext
{
    private readonly IPipelineBuilder<TContext> _pipelineBuilder;

    public TelegramApplicationBuilder(IPipelineBuilder<TContext> pipelineBuilder, IRequiredUpdates requiredUpdates,
        IHostEnvironment environment, IServiceCollection services, ConfigurationManager configuration,
        ConfigureHostBuilder configureHostBuilder, ILoggingBuilder loggingBuilder)
    {
        _pipelineBuilder = pipelineBuilder;
        RequiredUpdates = requiredUpdates;
        Environment = environment;
        Services = services;
        Configuration = configuration;
        Logging = loggingBuilder;
        Host = configureHostBuilder;
    } 
    
    public IRequiredUpdates RequiredUpdates { get; }
    public IHostEnvironment Environment { get; }
    public IServiceCollection Services { get; }

    IConfiguration Abstractions.ITelegramApplicationBuilder.Configuration => Configuration;

    public ILoggingBuilder Logging { get; }
    public ConfigureHostBuilder Host { get; }
    public ConfigurationManager Configuration { get; }
    public IPipelineBuilder<TContext> Use<TMiddleware>() where TMiddleware : IMiddleware<TContext>
    {
        return _pipelineBuilder.Use<TMiddleware>();
    }

    public IPipelineBuilder<TContext> Use(Type middlewareType)
    {
        return _pipelineBuilder.Use(middlewareType);
    }

    public IPipelineBuilder<TContext> Use(IMiddleware<TContext> middleware)
    {
        return _pipelineBuilder.Use(middleware);
    }

    public IPipelineBuilder<TContext> Use(ParameterAsNextMiddlewareFactoryDelegate<TContext> middlewareFactory)
    {
        return _pipelineBuilder.Use(middlewareFactory);
    }

    public IPipelineBuilder<TContext> Use(ParameterWithServiceProviderAsNextMiddlewareFactoryDelegate<TContext> middlewareFactory)
    {
        return _pipelineBuilder.Use(middlewareFactory);
    }

    public IPipelineBuilder<TContext> Use(FuncAsNextMiddlewareDelegateWithServiceProvider<TContext> middlewareDelegateWithServiceProvider)
    {
        return _pipelineBuilder.Use(middlewareDelegateWithServiceProvider);
    }

    public IPipelineBuilder<TContext> Use(FuncAsNextMiddlewareDelegate<TContext> middlewareDelegate)
    {
        return _pipelineBuilder.Use(middlewareDelegate);
    }
}