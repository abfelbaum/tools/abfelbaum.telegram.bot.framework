using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Abfelbaum.Telegram.Bot.Framework.AspNetCore;

public interface ITelegramApplicationBuilder : Abfelbaum.Telegram.Bot.Framework.Abstractions.ITelegramApplicationBuilder
{
    /// <summary>
    ///     A collection of logging providers for the application to compose. This is useful for adding new logging providers.
    /// </summary>
    public ILoggingBuilder Logging { get; }

    /// <summary>
    ///     An <see cref="IHostBuilder" /> for configuring host specific properties, but not building.
    /// </summary>
    public ConfigureHostBuilder Host { get; }

    /// <summary>
    ///     A collection of configuration providers for the application to compose. This is useful for adding new configuration
    ///     sources and providers.
    /// </summary>
    public new ConfigurationManager Configuration { get; }
}