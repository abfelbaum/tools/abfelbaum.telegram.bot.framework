using Abfelbaum.Telegram.Bot.Framework.DependencyInjection;
using Abfelbaum.Telegram.Bot.Framework.Services;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Abfelbaum.Telegram.Bot.Framework.AspNetCore;

public static class WebApplicationExtensions
{
    public static TelegramApplicationBuilder<TContext> AddTelegramAspNetServices<TContext>(
        this WebApplicationBuilder builder)
        where TContext : DefaultContext
    {
        builder.Services.AddControllers();

        var requiredUpdates = new RequiredUpdates();

        DependencyInjection.TelegramApplicationBuilder<TContext> applicationBuilder =
            builder.Services.AddTelegramDependencyInjectionServices<TContext>(builder.Environment,
                builder.Configuration,
                requiredUpdates);

        return new TelegramApplicationBuilder<TContext>(applicationBuilder, requiredUpdates, builder.Environment,
            builder.Services, builder.Configuration, builder.Host, builder.Logging);
    }
}