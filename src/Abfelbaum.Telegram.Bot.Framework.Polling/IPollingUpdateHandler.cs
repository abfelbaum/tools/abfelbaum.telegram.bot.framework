using Telegram.Bot.Polling;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Polling;

public interface IPollingUpdateHandler : IUpdateHandler
{
    public UpdateType[]? AllowedUpdates { get; }
}