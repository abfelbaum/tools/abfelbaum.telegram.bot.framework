using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Polling;

namespace Abfelbaum.Telegram.Bot.Framework.Polling;

public class PollingService : IHostedService
{
    private readonly ITelegramBotClient _client;
    private readonly ILogger<PollingService> _logger;
    private readonly ReceiverOptions _options;
    private readonly IPollingUpdateHandler _updateHandler;
    private CancellationTokenSource _tokenSource = null!;

    public PollingService(ITelegramBotClient client, IPollingUpdateHandler updateHandler,
        ILogger<PollingService> logger, IOptions<ReceiverOptions> options)
    {
        _client = client;
        _updateHandler = updateHandler;
        _logger = logger;
        _options = options.Value;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _tokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

        _logger.LogInformation("Starting update polling... ");

        _client.StartReceiving(_updateHandler, new ReceiverOptions
        {
            AllowedUpdates = _updateHandler.AllowedUpdates,
            Limit = _options.Limit,
            Offset = _options.Offset,
            DropPendingUpdates = _options.DropPendingUpdates
        }, cancellationToken);

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Shutting down update polling...");
        _tokenSource.Cancel();
        return Task.CompletedTask;
    }
}