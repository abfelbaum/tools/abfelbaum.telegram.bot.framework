using System;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.Polling;

public class PollingUpdateHandler : IPollingUpdateHandler
{
    private readonly IInternalUpdateHandler _updateHandler;

    public PollingUpdateHandler(IInternalUpdateHandler updateHandler)
    {
        _updateHandler = updateHandler;
    }

    public Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        return _updateHandler.HandleUpdateAsync(update, cancellationToken);
    }

    public Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
    {
        return _updateHandler.HandleErrorAsync(exception, cancellationToken);
    }

    public UpdateType[]? AllowedUpdates => _updateHandler.AllowedUpdates;
}