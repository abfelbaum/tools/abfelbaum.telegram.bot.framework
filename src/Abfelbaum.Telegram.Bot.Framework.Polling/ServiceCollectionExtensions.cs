using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Telegram.Bot.Polling;

namespace Abfelbaum.Telegram.Bot.Framework.Polling;

public static class ServiceCollectionExtensions
{
    public static OptionsBuilder<ReceiverOptions> AddPolling(this IServiceCollection services)
    {
        services.TryAddTransient<IPollingUpdateHandler, PollingUpdateHandler>();

        services.AddHostedService<PollingService>();

        return services.AddOptions<ReceiverOptions>();
    }

    public static void AddPolling(this IServiceCollection services, Action<ReceiverOptions> configureOptions)
    {
        OptionsBuilder<ReceiverOptions> optionsBuilder = services.AddPolling();

        optionsBuilder.Configure(configureOptions);
    }

    public static void AddPolling(this IServiceCollection services, IConfiguration configuration)
    {
        OptionsBuilder<ReceiverOptions> optionsBuilder = services.AddPolling();
        optionsBuilder.Bind(configuration);
    }
}