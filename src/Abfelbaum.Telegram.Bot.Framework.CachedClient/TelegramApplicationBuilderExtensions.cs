using System;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Abfelbaum.Telegram.Bot.Framework.CachedClient;

public static class TelegramApplicationBuilderExtensions
{
    public static ITelegramApplicationBuilder AddCachedClient(this ITelegramApplicationBuilder builder)
    {
        return builder.AddCachedClient(_ => { });
    }

    public static ITelegramApplicationBuilder AddCachedClient(this ITelegramApplicationBuilder builder,
        Action<MemoryCacheOptions> configureMemoryCacheOptions)
    {
        return builder.AddCachedClient(configureMemoryCacheOptions, options =>
        {
            options.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5);
        });
    }

    public static ITelegramApplicationBuilder AddCachedClient(this ITelegramApplicationBuilder builder,
        Action<MemoryCacheOptions> configureMemoryCacheOptions,
        Action<MemoryCacheEntryOptions> configureMemoryCacheEntryOptions)
    {
        builder.Services.TryAddTransient<ICachedTelegramBotClient, CachedTelegramBotClient>();
        builder.Services.AddMemoryCache(configureMemoryCacheOptions);
        builder.Services.Configure(configureMemoryCacheEntryOptions);

        return builder;
    }
}