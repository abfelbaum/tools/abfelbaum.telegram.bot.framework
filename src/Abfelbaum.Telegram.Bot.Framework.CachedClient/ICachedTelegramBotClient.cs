using Telegram.Bot;

namespace Abfelbaum.Telegram.Bot.Framework.CachedClient;

public interface ICachedTelegramBotClient : ITelegramBotClient;