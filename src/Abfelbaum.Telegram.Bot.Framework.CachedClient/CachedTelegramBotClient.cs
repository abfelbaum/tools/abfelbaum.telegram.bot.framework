﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Requests.Abstractions;

namespace Abfelbaum.Telegram.Bot.Framework.CachedClient;

public class CachedTelegramBotClient : ICachedTelegramBotClient
{
    private readonly ITelegramBotClient _botClient;
    private readonly IMemoryCache _cache;
    private readonly MemoryCacheEntryOptions _cacheEntryOptions;

    public CachedTelegramBotClient(ITelegramBotClient botClient, IMemoryCache cache, IOptions<MemoryCacheEntryOptions> cacheEntryOptions)
    {
        _botClient = botClient;
        _cache = cache;
        _cacheEntryOptions = cacheEntryOptions.Value;
    }

    public async Task<TResponse> MakeRequestAsync<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = new())
    {
        object? id;

        switch (request)
        {
            case IChatTargetable chatTarget:
                id = chatTarget.ChatId;
                break;
            
            case IUserTargetable userTarget:
                id = userTarget.UserId;
                break;
            default:
                return await _botClient.MakeRequestAsync(request, cancellationToken);
        }

        string cacheKey = $"{request.GetType()}-{id}";

        if (_cache.TryGetValue(cacheKey, out TResponse? cachedResponse) && cachedResponse is not null)
        {
            return cachedResponse;
        }

        TResponse response = await _botClient.MakeRequestAsync(request, cancellationToken);
        
        _cache.Set(cacheKey, response, _cacheEntryOptions);

        return response;
    }

    public async Task<bool> TestApiAsync(CancellationToken cancellationToken = new())
    {
        return await _botClient.TestApiAsync(cancellationToken);
    }

    public async Task DownloadFileAsync(string filePath, Stream destination,
        CancellationToken cancellationToken = new())
    {
        await _botClient.DownloadFileAsync(filePath, destination, cancellationToken);
    }

    public bool LocalBotServer => _botClient.LocalBotServer;

    public long? BotId => _botClient.BotId;

    public TimeSpan Timeout
    {
        get => _botClient.Timeout;
        set => _botClient.Timeout = value;
    }

    public IExceptionParser ExceptionsParser
    {
        get => _botClient.ExceptionsParser;
        set => _botClient.ExceptionsParser = value;
    }

    public event AsyncEventHandler<ApiRequestEventArgs>? OnMakingApiRequest
    {
        add => _botClient.OnMakingApiRequest += value;
        remove => _botClient.OnMakingApiRequest -= value;
    }

    public event AsyncEventHandler<ApiResponseEventArgs>? OnApiResponseReceived
    {
        add => _botClient.OnApiResponseReceived += value;
        remove => _botClient.OnApiResponseReceived -= value;
    }
}