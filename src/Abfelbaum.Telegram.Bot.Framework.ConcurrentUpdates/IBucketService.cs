namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public interface IBucketService
{
    public long GetBucket(long? id, params long?[] ids);
}