using System.Threading.Tasks;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public class ConcurrencyOptions
{
    public ConcurrencyType Concurrency { get; set; } = ConcurrencyType.UserAndChat;
    public int MaxDegreeOfParallelism { get; set; } = 0;
    public TaskScheduler TaskScheduler { get; set; } = TaskScheduler.Default;
}