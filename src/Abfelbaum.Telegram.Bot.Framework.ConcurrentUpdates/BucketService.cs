using System.Collections.Generic;
using System.Linq;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public class BucketService : IBucketService
{
    public long GetBucket(long? id, params long?[] ids)
    {
        var list = new List<long?>(ids) { id };

        return list.Select(x => x is null ? 0 : (long)x.GetHashCode())
            .Aggregate((long)23, (hash, itemHash) => hash * 31 + itemHash);
    }
}