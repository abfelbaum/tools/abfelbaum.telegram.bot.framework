using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;
using Microsoft.Extensions.Options;
using Middlewares;
using SharpCollections.Generic;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public class UpdateScheduler : IUpdateScheduler, IAsyncDisposable
{
    private readonly IBucketService _bucketService;
    private readonly ConcurrencyOptions _options;
    private readonly WorkScheduler<NextMiddleware> _updateScheduler;

    public UpdateScheduler(IBucketService bucketService, IOptions<ConcurrencyOptions> options)
    {
        _bucketService = bucketService;
        _options = options.Value;
        _updateScheduler = new WorkScheduler<NextMiddleware>(WorkRoutine, _options.MaxDegreeOfParallelism, _options.TaskScheduler);
    }

    async ValueTask IAsyncDisposable.DisposeAsync()
    {
        await _updateScheduler.StopAndWaitForCompletionAsync().ConfigureAwait(false);
    }

    public Task Schedule(NextMiddleware next, Update update)
    {
        _updateScheduler.Enqueue(next, _bucketService.GetBucket(update, _options.Concurrency));

        return Task.CompletedTask;
    }

    private static async Task WorkRoutine(NextMiddleware next)
    {
        await next().ConfigureAwait(false);
    }
}