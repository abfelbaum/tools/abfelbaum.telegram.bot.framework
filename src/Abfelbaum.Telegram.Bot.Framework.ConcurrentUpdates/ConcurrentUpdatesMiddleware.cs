using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Middlewares;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public class ConcurrentUpdatesMiddleware<TContext> : ITelegramMiddleware<TContext>
    where TContext : DefaultContext
{
    private readonly IUpdateScheduler _scheduler;

    public ConcurrentUpdatesMiddleware(IUpdateScheduler scheduler)
    {
        _scheduler = scheduler;
    }

    public Task InvokeAsync(TContext context, NextMiddleware next, CancellationToken cancellationToken = new())
    {
        return _scheduler.Schedule(next, context.Update);
    }
}