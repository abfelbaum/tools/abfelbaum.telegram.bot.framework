using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;

public static class ApplicationBuilderExtensions
{
    /// <summary>
    ///     Enables to handle multiple updates from multiple chats simultaneously. Updates of the same chat will be not handled
    ///     simultaneously.
    ///     Should be placed after stuff that is independent of the default update handling pipeline.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="TContext"></typeparam>
    /// <returns></returns>
    public static IApplicationBuilder<TContext> UseConcurrentUpdates<TContext>(
        this IApplicationBuilder<TContext> builder)
        where TContext : DefaultContext
    {
        builder.Use<ConcurrentUpdatesMiddleware<TContext>>();

        return builder;
    }
}