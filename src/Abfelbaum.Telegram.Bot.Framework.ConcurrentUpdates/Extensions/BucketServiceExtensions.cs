using System;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;

public static class BucketServiceExtensions
{
    public static long GetBucket(this IBucketService bucketService, Update update, ConcurrencyType concurrencyType) =>
        update.Type switch
        {
            UpdateType.Unknown => bucketService.GetBucket(concurrencyType),

            UpdateType.Message => bucketService.GetBucket(concurrencyType, update.Message?.Chat.Id,
                update.Message?.From?.Id),

            UpdateType.InlineQuery => bucketService.GetBucket(concurrencyType, null, update.InlineQuery?.From.Id),

            UpdateType.ChosenInlineResult => bucketService.GetBucket(concurrencyType, null,
                update.ChosenInlineResult?.From.Id),

            UpdateType.CallbackQuery when update.CallbackQuery?.Message is Message message => bucketService.GetBucket(
                concurrencyType, message.Chat.Id, update.CallbackQuery?.From.Id),

            UpdateType.EditedMessage => bucketService.GetBucket(concurrencyType, update.EditedMessage?.Chat.Id,
                update.EditedMessage?.From?.Id),

            UpdateType.ChannelPost => bucketService.GetBucket(concurrencyType, update.ChannelPost?.Chat.Id,
                update.ChannelPost?.From?.Id),

            UpdateType.EditedChannelPost => bucketService.GetBucket(concurrencyType, update.EditedChannelPost?.Chat.Id,
                update.EditedChannelPost?.From?.Id),

            UpdateType.ShippingQuery => bucketService.GetBucket(concurrencyType, null, update.ShippingQuery?.From.Id),

            UpdateType.PreCheckoutQuery => bucketService.GetBucket(concurrencyType, null,
                update.PreCheckoutQuery?.From.Id),

            UpdateType.Poll => bucketService.GetBucket(concurrencyType),

            UpdateType.PollAnswer => bucketService.GetBucket(concurrencyType, null, update.PollAnswer?.User?.Id),

            UpdateType.MyChatMember => bucketService.GetBucket(concurrencyType, update.MyChatMember?.Chat.Id,
                update.MyChatMember?.From.Id),

            UpdateType.ChatMember => bucketService.GetBucket(concurrencyType, update.ChatMember?.Chat.Id,
                update.ChatMember?.From.Id),

            UpdateType.ChatJoinRequest => bucketService.GetBucket(concurrencyType, update.ChatJoinRequest?.Chat.Id,
                update.ChatJoinRequest?.From.Id),

            _ => throw new ArgumentOutOfRangeException(nameof(update.Type), update.Type, null)
        };

    public static long GetBucket(this IBucketService bucketService, ConcurrencyType concurrencyType,
        long? chatId = null, long? userId = null)
    {
        var concurrencyTypeValue = (long)concurrencyType;
        return concurrencyType switch
        {
            ConcurrencyType.Chat => bucketService.GetBucket(concurrencyTypeValue, chatId, null),
            ConcurrencyType.User => bucketService.GetBucket(concurrencyTypeValue, null, userId),
            ConcurrencyType.UserAndChat => bucketService.GetBucket(concurrencyTypeValue, chatId, userId),
            _ => throw new ArgumentOutOfRangeException(nameof(concurrencyType), concurrencyType, null)
        };
    }
}