using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddConcurrentUpdates(this IServiceCollection services,
        Action<ConcurrencyOptions>? options = null)
    {
        services.AddOptions();

        if (options is not null)
        {
            services.Configure(options);
        }

        services.TryAddTransient<IBucketService, BucketService>();
        services.TryAddSingleton<IUpdateScheduler, UpdateScheduler>();

        return services;
    }
}