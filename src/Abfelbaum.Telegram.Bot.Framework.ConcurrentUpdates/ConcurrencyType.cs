namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public enum ConcurrencyType
{
    Chat,
    User,
    UserAndChat
}