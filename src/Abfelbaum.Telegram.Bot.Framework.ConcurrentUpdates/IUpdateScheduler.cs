using System.Threading.Tasks;
using Middlewares;
using Telegram.Bot.Types;

namespace Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates;

public interface IUpdateScheduler
{
    public Task Schedule(NextMiddleware next, Update update);
}