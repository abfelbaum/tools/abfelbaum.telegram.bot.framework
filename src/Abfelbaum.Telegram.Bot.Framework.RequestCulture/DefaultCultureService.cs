﻿using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Abfelbaum.Telegram.Bot.Framework.RequestCulture;

public class DefaultCultureService : IHostedService
{
    private readonly RequestCultureOptions _options;

    public DefaultCultureService(IOptions<RequestCultureOptions> options)
    {
        _options = options.Value;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        CultureInfo.DefaultThreadCurrentCulture = _options.DefaultCulture;

        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}