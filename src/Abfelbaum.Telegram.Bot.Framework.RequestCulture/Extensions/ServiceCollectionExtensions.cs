using System;
using System.Globalization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;

public static class ServiceCollectionExtensions
{
    public static OptionsBuilder<RequestCultureOptions> AddRequestCulture(this IServiceCollection services)
    {
        services.AddHostedService<DefaultCultureService>();
        services.AddOptions();

        OptionsBuilder<RequestCultureOptions> optionsBuilder = services.AddOptions<RequestCultureOptions>();

        return optionsBuilder;
    }

    public static void AddRequestCulture(this IServiceCollection services,
        Action<RequestCultureOptions> configureOptions)
    {
        OptionsBuilder<RequestCultureOptions> optionsBuilder = services.AddRequestCulture();

        optionsBuilder.Configure(configureOptions);
    }
}