using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;

namespace Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;

public static class ApplicationBuilderExtensions
{
    public static IApplicationBuilder<TContext> UseRequestCulture<TContext>(this IApplicationBuilder<TContext> app)
        where TContext : DefaultContext
    {
        app.Use<RequestCultureMiddleware<TContext>>();

        return app;
    }
}