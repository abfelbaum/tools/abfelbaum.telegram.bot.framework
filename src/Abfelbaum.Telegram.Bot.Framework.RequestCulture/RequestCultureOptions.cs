using System.Globalization;

namespace Abfelbaum.Telegram.Bot.Framework.RequestCulture;

public class RequestCultureOptions
{
    public CultureInfo DefaultCulture { get; set; } = CultureInfo.InvariantCulture;
}