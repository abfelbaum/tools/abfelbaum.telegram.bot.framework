using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Microsoft.Extensions.Options;
using Middlewares;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Abfelbaum.Telegram.Bot.Framework.RequestCulture;

public class RequestCultureMiddleware<TContext> : ITelegramMiddleware<TContext> where TContext : DefaultContext
{
    private readonly RequestCultureOptions _options;

    public RequestCultureMiddleware(IOptions<RequestCultureOptions> options)
    {
        _options = options.Value;
    }

    public Task InvokeAsync(TContext context, NextMiddleware next,
        CancellationToken cancellationToken = new())
    {
        CultureInfo culture = GetCulture(context.Update);

        CultureInfo.CurrentCulture = culture;
        CultureInfo.CurrentUICulture = culture;

        return next();
    }

    private CultureInfo GetCulture(Update update)
    {
        string? cultureName = update.Type switch
        {
            UpdateType.Message => update.Message?.From?.LanguageCode,
            UpdateType.CallbackQuery when update.CallbackQuery?.Message is Message message => message.From?.LanguageCode,
            UpdateType.EditedMessage => update.EditedMessage?.From?.LanguageCode,
            UpdateType.ChannelPost => update.ChannelPost?.From?.LanguageCode,
            UpdateType.EditedChannelPost => update.EditedChannelPost?.From?.LanguageCode,
            UpdateType.MyChatMember => update.MyChatMember?.From.LanguageCode,
            UpdateType.ChatMember => update.ChatMember?.From.LanguageCode,
            UpdateType.InlineQuery => update.InlineQuery?.From.LanguageCode,
            UpdateType.ChosenInlineResult => update.ChosenInlineResult?.From.LanguageCode,
            UpdateType.ShippingQuery => update.ShippingQuery?.From.LanguageCode,
            UpdateType.PreCheckoutQuery => update.PreCheckoutQuery?.From.LanguageCode,
            UpdateType.PollAnswer => update.PollAnswer?.User?.LanguageCode,
            UpdateType.Unknown => null,

            UpdateType.Poll => null,
            _ => null
        };

        if (string.IsNullOrWhiteSpace(cultureName))
        {
            return _options.DefaultCulture;
        }

        return CultureInfo.GetCultureInfo(cultureName);
    }
}